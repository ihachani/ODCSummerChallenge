﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class menuAnimationManager : MonoBehaviour {

	public Transform tactileB;
	public Transform cardBaordB;
	public GameObject menuBTM;
	public GameObject menuBG;
	public GameObject menuBn;
	[SerializeField]
	private List<Animator> menuAnimators;
	[SerializeField]
	private string idleAnimationTriggerName;

	const float WAIT_BETWEEN_MENUS = 0.65f;

	void Awake(){
		tactileB.GetComponent<Animator> ().SetTrigger ("Apear");
		cardBaordB.GetComponent<Animator> ().SetTrigger ("Apear");
	}
	// Use this for initialization
	void Start () {
		//menuBTM.SetActive (false);
//		menuBn.SetActive (false);

	}
	
	// Update is called once per frame
	void Update () {
	
	}




	public void choixCbTlAppear(){
		SecondMenuDisappCorouine ();
		tactileB.GetComponent<Animator> ().SetTrigger ("Apear");
		cardBaordB.GetComponent<Animator> ().SetTrigger ("Apear");

	}

	void SecondMenuDisappCorouine(){
		foreach (var animator in menuAnimators) {
			animator.SetTrigger(idleAnimationTriggerName);
		}
		for (int i = 0; i < menuBn.transform.childCount; i++) {
			menuBn.transform.GetChild(i).gameObject.SetActive(false);
		}
		
	}

	public void choixCbTlDesappear(){

		VRMobileChoiceMenuDisapear ();
//		StartCoroutine ("SecondMenuCorouine");
//		StartCoroutine ("buttonsAppCorouine");
	}

	void VRMobileChoiceMenuDisapear ()
	{
		tactileB.GetComponent<Animator> ().SetTrigger ("Disapear");
		cardBaordB.GetComponent<Animator> ().SetTrigger ("Disapear");
	}



	IEnumerator SecondMenuCorouine(){
		yield return new WaitForSeconds (WAIT_BETWEEN_MENUS);

		menuBTM.SetActive (true);


	}
	IEnumerator buttonsAppCorouine(){
		yield return new WaitForSeconds (2f);
		menuBn.SetActive (true);
	}
	public void StartGameWithCardBoardControl()
	{
		Game.current=new Game();
		Game.current.config.VR = true;
			
	}

    public void StartGameWithTactileControl()
	{
		Game.current=new Game();
		Game.current.config.VR = false;
			
	}
    public void OnStartButtonClick()
	{
		SaveLoad.Save();
		Application.LoadLevel("RoomFinals");

	}
}

﻿using UnityEngine;
using System.Collections;

public class ControlChoice : MonoBehaviour {
    [SerializeField]
    private GameObject TactileControl;
    [SerializeField]
    private GameObject CardBoardControl;
    // Use this for initialization
	void Awake () {
        SaveLoad.Load();
        bool choice=SaveLoad.savedGames[SaveLoad.savedGames.Count-1].config.VR;
        Debug.Log(choice);
        ActiveControl(choice);
	}

    private void ActiveControl(bool choice)
    {
        if (choice == true)
        {
            CardBoardControl.SetActive(true);
        }
        else 
        {
            TactileControl.SetActive(true);
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;

namespace OrangeSummerChallenge.CharcterController
{
    public class LookAroundMobileController : MonoBehaviour
    {
        [SerializeField]
        private Look lookScript;
        [SerializeField]
        private Camera camera;
        void Start()
        {
            lookScript.Init(transform, camera.transform);
        }
        void FixedUpdate()
        {
            lookScript.LookRotation(transform, camera.transform);
        }
    }
}

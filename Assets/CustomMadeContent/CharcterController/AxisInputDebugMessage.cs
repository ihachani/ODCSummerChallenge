﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.UI;

namespace OrangeSummerChallenge.CharcterController
{
    public class AxisInputDebugMessage : MonoBehaviour
    {
        [SerializeField]
        private Text debugTextField;
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void FixedUpdate()
        {
            //Debug.Log("CrossPltaformInput : " + CrossPlatformInputManager.GetAxis("Mouse X"));
            //Debug.Log("Input : " + Input.GetAxis("Mouse X"));
            debugTextField.text = "Horizontal: " + CrossPlatformInputManager.GetAxis("Horizontal") + "\n";
            debugTextField.text += "Vertical: " + CrossPlatformInputManager.GetAxis("Vertical") + "\n";
            debugTextField.text += "Mouse X: " + CrossPlatformInputManager.GetAxis("Mouse X") + "\n";
            debugTextField.text += "Mouse Y: " + CrossPlatformInputManager.GetAxis("Mouse Y");
        }
    }

}
﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Utility;
using UnityStandardAssets.CrossPlatformInput;

namespace OrangeSummerChallenge.CharcterController
{
    [RequireComponent(typeof(CharacterController))]
    public class SimplePlayerController : MonoBehaviour
    {
        [SerializeField]
        private float speed;
        [SerializeField]
        private float turnSpeed;
        [SerializeField]
        private Camera camera;
        [SerializeField]
        private Look look;
        [SerializeField]
        private CurveControlledBob headBob = new CurveControlledBob();
        [SerializeField]
        private float stepInterval;
        CharacterController characterController;
        // Use this for initialization
        void Start()
        {
            characterController = GetComponent<CharacterController>();
            look.Init(transform, camera.transform);
            headBob.Setup(camera, stepInterval);
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            look.LookRotation(transform, camera.transform);
            Vector3 vel = GetKeyboardInput();
            characterController.SimpleMove(vel);
            //Debug.Log("Velocity: " + characterController.velocity.magnitude);
            //headBob.DoHeadBob(speed);
        }

        private Vector3 GetKeyboardInput()
        {
            Vector3 vel = transform.forward * CrossPlatformInputManager.GetAxis("Vertical") * speed;
            vel += transform.right * CrossPlatformInputManager.GetAxis("Horizontal") * speed;
            return vel;
        }
    }

}
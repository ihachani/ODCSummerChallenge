﻿using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace OrangeSummerChallenge.CharcterController
{
    [Serializable]
    public class Look
    {
        #region PrivateFields
        #region SerialFields
        [SerializeField]
        private float XSensitivity = 2f;
        [SerializeField]
        private float YSensitivity = 2f;
        [SerializeField]
        private bool clampVerticalRotation = true;
        [SerializeField]
        private float MinimumX = -90F;
        [SerializeField]
        private float MaximumX = 90F;
        [SerializeField]
        private bool smooth;
        [SerializeField]
        private float smoothTime = 5f;
        #endregion


        private Quaternion m_CharacterTargetRot;
        private Quaternion m_CameraTargetRot;
        #endregion


        public void Init(Transform character, Transform camera)
        {
            m_CharacterTargetRot = character.localRotation;
            m_CameraTargetRot = camera.localRotation;
        }


        public void LookRotation(Transform character, Transform camera)
        {
            Vector2 rotation = GetMouseInput();
            //Vector2 rotation = GetKeyboardInput();

            CalculateTargetRotation(rotation);

            if (clampVerticalRotation)
                CalculateRotationWithClampVertical();

            if (smooth)
            {
                RotateUsingSmooth(character, camera);
            }
            else
            {
                RotateWithoutSmooth(character, camera);
            }
        }
        /// <summary>
        /// Return the Rotation around the x and y axis.Reading the mouse input.
        /// </summary>
        /// <returns></returns>
        private Vector2 GetMouseInput()
        {
            float yRot = CrossPlatformInputManager.GetAxis("Mouse X") * XSensitivity;
            float xRot = CrossPlatformInputManager.GetAxis("Mouse Y") * YSensitivity;
            return new Vector2(xRot, yRot);
        }
        /// <summary>
        /// Calculate the base target Rotation.
        /// </summary>
        /// <param name="rotation">The Rotation values from the input.</param>
        private void CalculateTargetRotation(Vector2 rotation)
        {
            m_CharacterTargetRot *= Quaternion.Euler(0f, rotation.y, 0f);
            m_CameraTargetRot *= Quaternion.Euler(-rotation.x, 0f, 0f);
        }
        /// <summary>
        /// Calculate Rotation clamping Vertical.
        /// </summary>
        private void CalculateRotationWithClampVertical()
        {
            m_CameraTargetRot = ClampRotationAroundXAxis(m_CameraTargetRot);
        }
        // I tested this for a while and I think this limits the rotation angle.
        Quaternion ClampRotationAroundXAxis(Quaternion q)
        {
            q.x /= q.w;
            q.y /= q.w;
            q.z /= q.w;
            q.w = 1.0f;

            float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);

            angleX = Mathf.Clamp(angleX, MinimumX, MaximumX);

            q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

            return q;
        }
        /// <summary>
        /// Change The local roation of the charcter and the camera to the target Rotation Without using smooth.
        /// </summary>
        /// <param name="character"></param>
        /// <param name="camera"></param>
        private void RotateWithoutSmooth(Transform character, Transform camera)
        {
            character.localRotation = m_CharacterTargetRot;
            camera.localRotation = m_CameraTargetRot;
        }
        /// <summary>
        /// Change The local roation of the charcter and the camera to the target Rotation using smooth.
        /// </summary>
        /// <param name="character"></param>
        /// <param name="camera"></param>
        private void RotateUsingSmooth(Transform character, Transform camera)
        {
            character.localRotation = Quaternion.Slerp(character.localRotation, m_CharacterTargetRot,
                smoothTime * Time.deltaTime);
            camera.localRotation = Quaternion.Slerp(camera.localRotation, m_CameraTargetRot,
                smoothTime * Time.deltaTime);
        }

        /// <summary>
        /// Return the Rotation around the x and y axis.Reading the keyboard input.
        /// </summary>
        private Vector2 GetKeyboardInput()
        {
            float yRot = CrossPlatformInputManager.GetAxis("Horizontal") * 90 * Time.deltaTime;
            float xRot = CrossPlatformInputManager.GetAxis("Vertical") * 90 * Time.deltaTime;
            return new Vector2(xRot, yRot);
        }



    }
}

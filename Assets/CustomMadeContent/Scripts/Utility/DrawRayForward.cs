﻿using UnityEngine;
using System.Collections;

public class DrawRayForward : MonoBehaviour
{
    [SerializeField]
    [Range(0,1000)]
    private float length = 10;
    [SerializeField]
    private Color color = Color.green;
    // Update is called once per frame
    void Update()
    {
        DrawRay();
    }

    private void DrawRay()
    {
        Vector3 forward = CreateDirVector();
        Debug.DrawRay(transform.position, forward, color);
    }

    private Vector3 CreateDirVector()
    {
        Vector3 forward = transform.TransformDirection(Vector3.forward) * length;
        return forward;
    }
}

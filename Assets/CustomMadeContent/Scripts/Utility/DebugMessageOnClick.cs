﻿using UnityEngine;
using System.Collections;

namespace OrangeSummerChallenge.Utility
{
    public class DebugMessageOnClick : MonoBehaviour
    {
        [SerializeField]
        private string messageLog;
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        public void OnClick()
        {
            Debug.Log(messageLog);
        }
    }
    
}
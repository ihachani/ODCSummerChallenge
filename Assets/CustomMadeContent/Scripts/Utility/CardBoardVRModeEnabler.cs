﻿using UnityEngine;
using System.Collections;
namespace OrangeSummerChallenge.Utility
{
    /// <summary>
    /// <para>This class is used to reactivate the vr mode on a cardboard when scene Starts.</para>
    /// <para>This is due to a bug in the cardboard class when switching from cardboard to non cardboard.</para>
    /// </summary>
    public class CardBoardVRModeEnabler : MonoBehaviour
    {
        [Tooltip("Cardboard To Enable the VR Mode On")]
        [SerializeField]
        private Cardboard cardboardToEnable;
        // Use this for initialization
        void Start()
        {
            EnableVRMode();
        }

        private void EnableVRMode()
        {
            cardboardToEnable.VRModeEnabled = true;
        }

    }
    
}
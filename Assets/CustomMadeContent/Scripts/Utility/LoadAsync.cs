﻿using UnityEngine;
using System.Collections;

namespace OrangeSummerChallenge.Utility
{
    public class LoadAsync : MonoBehaviour
    {
        AsyncOperation async;
        /// <summary>
        /// The Update Is used to test weather or not we should activate the next scene.
        /// </summary>
        void Update()
        {

            if (async != null)
            {
                if (async.progress >= 0.9f)
                {
                    ActivateScene();
                }
            }
        }
        public void LoadNextLevel()
        {
            int nextLevel = GetNextLevelNumber();
            StartCoroutine(LoadLevelCouroutine(nextLevel));
        }
        private int GetNextLevelNumber()
        {
            int nextLevel = Application.loadedLevel + 1;
            Debug.Log("NextLevel: "+nextLevel);
            Debug.Log("NbrLevel: "+Application.levelCount);
            if (nextLevel < Application.levelCount)
            {
                return nextLevel;
            }
            else
            {
                Debug.Log("Attention c'est la derniere scene");
                return 0;
            }
                
            
        }
        IEnumerator LoadLevelCouroutine(int Level)
        {

            //Debug.LogWarning("ASYNC LOAD STARTED - " +
            //   "DO NOT EXIT PLAY MODE UNTIL SCENE LOADS... UNITY WILL CRASH");
            async = Application.LoadLevelAsync(Level);
            async.allowSceneActivation = false;
            yield return async;
        }
        IEnumerator ActivateSceneWhenDone()
        {
            while (async.progress <= 0.9f)
            {
                yield return new WaitForFixedUpdate();
            }
            ActivateScene();
        }
        public void LoadScene(int number)
        {
            StartCoroutine(LoadLevelCouroutine(number));
        }
        public void ActivateScene()
        {
            async.allowSceneActivation = true;
        }
    }
}

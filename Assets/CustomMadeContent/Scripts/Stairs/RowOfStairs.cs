﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using OrangeSummerChallenge.Focus.Actions.ColorSwap;
using OrangeSummerChallenge.Focus.Actions.SetWin;

public class RowOfStairs : MonoBehaviour
{

    [SerializeField]
    List<GameObject> Stairs;
    [SerializeField]
    List<GameObject> LoseColliders;
    [SerializeField]
    List<GameObject> ChosenColliders;



    /// <summary>
    /// This part is the Part that will choose one element from the row
    /// and then stores its index into chosen one
    /// </summary>
    private int chosenOne;  //stores the index

    //this method will choose a stair randomly
    private int ChooseAStair()
    {

        System.Random r = new System.Random();
        int index = r.Next(0, (Stairs.Count));
        return index;

    }

    //This method will return the chosen Element
    public GameObject ReturnChosen()
    {

        chosenOne = ChooseAStair();
        return Stairs[chosenOne];
    }


    //this method will return the not chosen one
    public List<GameObject> ReturnNotChosen()
    {
        List<GameObject> toReturn = new List<GameObject>();
        for (int i = 0; i < Stairs.Count; i++)
        {

            if (i != chosenOne)
            {
                toReturn.Add(Stairs[i]);
            }

        }
        return toReturn;

    }


    //Chosen Activator will activate the win collider and swap collider element from the Element
    // All the Action related to chosen object 
    // Will b stored in a specific collider 
    // that is inilially desactivated and it will be only activated for chosen ones
    public void ChosenActivator()
    {

        Debug.Log("Chosen Activated");
        ChosenColliders[chosenOne].SetActive(true);

    }




    // This Class Will Activate the 
    // The Lost Collider (once focused this object will end the game)

    public void NotChosenActivator()
    {

        for (int i = 0; i < LoseColliders.Count; i++)
        {
            if (i != chosenOne)
            {

                LoseColliders[i].SetActive(true);
            }


        }
    }


    /// <summary>
    /// This Part will Disactivate all the script reponsible for win and loose
    /// after the end of the game
    /// </summary>

    public void Desactivator()
    {
        Debug.Log("Row deactivated");
        for (int i = 0; i < Stairs.Count; i++)
        {
            Debug.Log("Row deactivated:" + LoseColliders[i].name);
            LoseColliders[i].SetActive(false);
            ChosenColliders[i].SetActive(false);


        }

    }

}

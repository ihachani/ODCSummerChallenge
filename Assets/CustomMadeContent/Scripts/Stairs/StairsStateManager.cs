﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic ;


namespace OrangeSummerChallenge.Stairs
{
    public class StairsStateManager : MonoBehaviour
    {

        [SerializeField]
        private StairsMaster master;

        [SerializeField]
        private List<AudioSource> audiosource ;

        [SerializeField]
        private AudioSource story;

        // Use this for initialization
        void Start()
        {

            story.Play();
            
            master.InitGame();
            
           

            Debug.Log("Game Started ...");
            StartCoroutine("AnimateChosen" , master.ReturnAllChosen());

         

        }


        
      
        
       


        IEnumerator AnimateChosen(List<GameObject> chosen) {

                yield return new WaitForSeconds(story.clip.length);
           // yield return new WaitForSeconds(2f);
            for (int i = 0; i < chosen.Count; i++) {

                chosen[i].GetComponent<Renderer>().material.SetColor("_Color", Color.black);
                audiosource[i].Play();

                yield return new WaitForSeconds(0.5f);
                chosen[i].GetComponent<Renderer>().material.SetColor("_Color", Color.white);

             
            }

            master.FirstRowActivator();
            yield return new WaitForSeconds(0f);

        
        }






      
    }
}
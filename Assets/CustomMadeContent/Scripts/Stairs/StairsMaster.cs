﻿using OrangeSummerChallenge.Actions;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OrangeSummerChallenge.Stairs
{
    [Serializable]
    class StairsMaster : MonoBehaviour
    {


        [SerializeField]
        List<RowOfStairs> rows;

        [SerializeField]
        private DestructionAnimationStarter starterDestruction;
        [SerializeField]
        private NavMeshDecactivator navMesDisabler;
        [SerializeField]
        private LoadSceneAction loadSceneAction;
        [SerializeField]
        private MoveToTransform SuccessTarget;

        //This Variable Will Store The value of the current row To Play
        private int index;
        // This List Will Store All the chosen Elements
        private List<GameObject> chosenElementsList;


        /// <summary>
        /// This Part of Strais Master is charged to choose 1 element 
        /// for each Row and the to add it to the attribute chosen Element List
        /// That will store all the Element Chosen
        /// </summary>

        ///This Method Is Charged To Return the Chosen Elements List
        public List<GameObject> ReturnAllChosen()
        {

            return chosenElementsList;

        }

        // This Method was Designed to Initiate the list of chosen Elements
        public void GetAllChosen()
        {
            for (int i = 0; i < rows.Count; i++)
            {
                chosenElementsList.Add(rows[i].ReturnChosen() as GameObject);

            }
        }

        /// <summary>
        /// This Part of Class is the events of the Game 
        /// Init Game make initilaisation of the game the game 
        /// </summary>




        //Init Game Start by choosing Random Elements 1 element from each Row
        //And then Activate the first Element of the first Row
        public void InitGame()
        {

            index = 0;
            chosenElementsList = new List<GameObject>();
            GetAllChosen();
        }


        // This Function is charged to start the first
        //Row
        public void FirstRowActivator()
        {

            //activate First Row
            rows[index].ChosenActivator();
            rows[index].NotChosenActivator();
            Debug.Log("Value Of Index" + index);


        }


        /// <summary>
        /// This Part is used to Mnage The Advancement of The Game
        /// </summary>




        private void LevelActivator(int i)
        {


            rows[i].ChosenActivator();
            rows[i].NotChosenActivator();
            Debug.Log("Level Actvated " + i);

        }


        public void PlayGame()
        {
            index++;
            Debug.Log("Value Of Index" + index);

            if (index == rows.Count)
            {//Winn Action Here 
                Debug.Log("You Won");

                EndGame();
                SuccessTarget.Action();
            }

            else if (index < rows.Count)
            {
                Debug.Log("Level Activated " + index);
                rows[index-1].Desactivator();
                Debug.Log("Row dActivated " + (index -1));
                LevelActivator(index);

            }
            else
            {

                Debug.Log("Error In Pointer");
            }


        }


        /// <summary>
        /// This Method is charged of Ending The Games
        /// </summary>



        // End All the game Componement
        public void EndGame()
        {
            chosenElementsList.Clear(); //clears the list of All Elements
            RowsDesactivator();
        }


        // This Method Desactivates All The Chosen Elmenets
        public void RowsDesactivator()
        {

            for (int i = 0; i < rows.Count; i++)
            {

                rows[i].Desactivator();

            }
        }








        public void LoseGame()
        {
            Debug.Log("Lost Game..");
            EndGame();
            navMesDisabler.Action();
            
            starterDestruction.Action();
            StartCoroutine(Reload());
        }


        IEnumerator Reload() {
            yield return new WaitForSeconds(3f);
            Application.LoadLevel(Application.loadedLevel);
            yield return null;
        }

    }
}

﻿using UnityEngine;
using System.Collections;

namespace OrangeSummerChallenge.JsonSerialisation.SaveLevelBeforeQuit
{
    public class SaveLevelBeforeQuit : MonoBehaviour
    {
#if (UNITY_EDITOR || UNITY_ANDROID)
        public void OnApplicationQuit()
        {
            GameSettingsDict.InitSettingsDict();
            GameSettingsDict.ModifySettings(Pairing.Of(JsonKey.LevelTag, Application.loadedLevel));
            GameSettingsDict.Commit();
        }
#elif (UNITY_IOS)
        public void OnApplicationPause(bool pause)
        {
            if (pause)
            {
                GameSettingsDict.ModifySettings(Pairing.Of(JsonKey.LevelTag, Application.loadedLevel));
                GameSettingsDict.Commit();
            }
            
        }
#endif



    }
}
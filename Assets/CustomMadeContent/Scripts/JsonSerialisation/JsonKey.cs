﻿using UnityEngine;
using System.Collections;

namespace OrangeSummerChallenge.JsonSerialisation
{
    public static class JsonKey
    {
        private const string settingsTag = "Settings";

        public static string SettingsTag
        {
            get { return settingsTag; }
        }

        private const string vrTag = "VR";

        public static string VRTag
        {
            get { return vrTag; }
        }

        private const string backgroundMusicTag = "BackgroundMusic";

        public static string BackgroundMusicTag
        {
            get { return backgroundMusicTag; }
        }

        private const string verticalSensitivityTag = "verticalSensitivity";

        public static string VerticalSensitivityTag
        {
            get { return verticalSensitivityTag; }
        }

        private const string horizentalSensitivityTag = "horizentalSensitivity";

        public static string HorizentalSensitivityTag
        {
            get { return horizentalSensitivityTag; }
        }

        private const string soundEffectsTag = "SoundEffects";

        public static string SoundEffectsTag
        {
            get { return soundEffectsTag; }
        }

        private const string postionTag = "Position";

        public static string PostionTag
        {
            get { return postionTag; }
        }
        private const string mainPostionTag = "MainPosition";

        public static string MainPostionTag
        {
            get { return mainPostionTag; }
        }


        private const string timeTag = "Time";

        public static string TimeTag
        {
            get { return timeTag; }
        }

        private const string levelTag = "Level";

        public static string LevelTag
        {
            get { return levelTag; }
        }
        private static int level = 0;

        public static int Level
        {
            get { return JsonKey.level; }
            set { JsonKey.level = value; }
        }


        private const string mysteriousManTag = "MysteriousMan";

        public static string MysteriousManTag
        {
            get { return mysteriousManTag; }
        }
        private const string fileName = "SaveLastPosition";

        public static string FileName
        {
            get { return fileName; }
        }
        private const string fileNameGameSettings = "GameSettings";

        public static string FileNameGameSettings
        {
            get { return fileNameGameSettings; }
        }
        private const string jsonExtenstion = ".JSON";

        public static string JsonExtenstion
        {
            get { return jsonExtenstion; }
        }

    }
    
}
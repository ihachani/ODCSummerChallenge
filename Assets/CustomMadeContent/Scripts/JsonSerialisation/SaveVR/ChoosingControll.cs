﻿using UnityEngine;
using System.Collections;

namespace OrangeSummerChallenge.JsonSerialisation.SaveVR
{
    
    /// <summary>
    /// Used to chose mobile or VR Mode.
    /// </summary>
    public class ChoosingControll : MonoBehaviour
    {
        [SerializeField]
        private Managers.SceneLoadManager sceneLoadManager;
        /// <summary>
        /// Sets the mode to cardboard and saves the choice into the JSON File.
        /// </summary>
        public void CardboardChoice()
        {
            GameSettingsDict.InitSettingsDict();
            GameSettingsDict.ModifySettings(Pairing.Of(JsonKey.VRTag, true));
            GameSettingsDict.Commit();
            sceneLoadManager.LoadNextScene();
        }
        /// <summary>
        /// Sets the mode to mobile and saves the choice into the JSON File.
        /// </summary>
        public void MobileChoice()
        {
            GameSettingsDict.InitSettingsDict();
            GameSettingsDict.ModifySettings(Pairing.Of(JsonKey.VRTag, false));
            GameSettingsDict.Commit();
            sceneLoadManager.LoadNextScene();
        }
        /// <summary>
        /// Loads the next scene and increases the the JSON counter by 1.
        /// </summary>
        public void LoadingNextScene()
        {
            Application.LoadLevel(Application.loadedLevel+1);
        }
    } 
}

﻿using UnityEngine;
using System.Collections;


namespace OrangeSummerChallenge.JsonSerialisation.SaveVR
{
    public class ActivateControll : MonoBehaviour
    {

        [SerializeField]
        private GameObject[] Cardboard;
        [SerializeField]
        private GameObject[] Mobile;


        public void Awake()
        {
            GameSettingsDict.LoadGameSettings();
            bool controll = GameOption.VR;
            Debug.Log("CardBoard : " + controll);

            foreach (GameObject mobile in Mobile)
            {
                mobile.SetActive(!controll);
            }
            foreach (GameObject cardboard in Cardboard)
            {
                cardboard.SetActive(controll);
            }


        }
    }
    
}
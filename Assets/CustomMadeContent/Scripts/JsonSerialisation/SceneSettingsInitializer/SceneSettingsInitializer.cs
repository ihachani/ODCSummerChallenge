﻿using UnityEngine;
using System.Collections;

namespace OrangeSummerChallenge.JsonSerialisation.SceneSettingsInitializer
{
    public class SceneSettingsInitializer : MonoBehaviour
    {
        void Awake()
        {
            GameSettingsDict.InitSettingsDict();
        }
    }
    
}
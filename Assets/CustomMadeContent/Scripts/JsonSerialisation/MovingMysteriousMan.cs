﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace OrangeSummerChallenge.JsonSerialisation
{
    public class MovingMysteriousMan : MonoBehaviour
    {
        [SerializeField]
        private SavingPosition savingPosition;
        private Dictionary<string, object> MainPositionDict;
        private int nbrItem;
        private Vector3 TargetPosition;
        [SerializeField]
        private float speed;
        // Use this for initialization
        void Start()
        {
            //MainPositionDict = SavingJsonFile.LoadJsonFile(JsonKey.Level,false)[JsonKey.LevelTag+JsonKey.Level] as Dictionary<string,object>;
            MainPositionDict = SavingJsonFile.LoadJsonFile(false);
            if (MainPositionDict == null)
            {
                this.gameObject.SetActive(false);
            }
            else
            {
                MainPositionDict = MainPositionDict[Application.loadedLevelName] as Dictionary<string, object>;
            }
            nbrItem = 0;
            TargetPosition = savingPosition.transform.position;
            speed = 2;
        }

        // Update is called once per frame
        void Update()
        {
            if (MainPositionDict != null)
            {
                if (nbrItem < MainPositionDict.Count)
                {
                    Dictionary<string, object> PosTimeDict = MainPositionDict[JsonKey.MainPostionTag + nbrItem] as Dictionary<string, object>;
                    float time = Convert.ToSingle(PosTimeDict[JsonKey.TimeTag + nbrItem]);
                    if (savingPosition.TimeNow >= time - 1)
                    {
                        TargetPosition = getPosition((List<object>)PosTimeDict[JsonKey.PostionTag + nbrItem]);
                        //Debug.Log("Target Position: " + TargetPosition.ToString());
                        nbrItem++;
                        //Debug.Log("position actuelle: " + this.transform.position);
                    }
                }
                this.transform.position = Vector3.MoveTowards(transform.position, TargetPosition, speed * Time.deltaTime);
            }
        }

        public Vector3 getPosition(List<object> list)
        {
            return new Vector3(Convert.ToSingle(list[0]), Convert.ToSingle(list[1]), Convert.ToSingle(list[2]));
        }
    }
}
﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System;

namespace OrangeSummerChallenge.JsonSerialisation
{
    public static class SavingJsonFile 
    {
        private static string path = Application.persistentDataPath + "/";
        //public static void SaveJsonFile(Dictionary<string, object> dict, bool append)
        //{
        //    string[] lines = System.IO.File.ReadAllLines(path);
        //    lines[JsonKey.Level] = Json.Serialize(dict);
        //    System.IO.File.WriteAllLines(path, lines);
        //}
        public static Dictionary<string, object> InitSettingsDictioanary()
        {
            Dictionary<string, object> SettingsDict = new Dictionary<string, object>() ;
            Dictionary<string, object> SettingDict = new Dictionary<string, object>();

            SettingDict.Add(JsonKey.VRTag, GameOption.VR);
            SettingDict.Add(JsonKey.SoundEffectsTag, GameOption.SoundEffects);
            SettingDict.Add(JsonKey.VerticalSensitivityTag, GameOption.VerticalSensitivity);
            SettingDict.Add(JsonKey.HorizentalSensitivityTag, GameOption.HorizentalSensitivity);
            SettingDict.Add(JsonKey.BackgroundMusicTag, GameOption.BackgroundMusic);
            SettingsDict.Add(JsonKey.SettingsTag, SettingDict);
            return SettingsDict;
        }


        public static string ConstructFileName(bool IsSettings)
        {
            string filename = "";
            if (IsSettings)
            {
                filename = path + JsonKey.FileNameGameSettings + JsonKey.JsonExtenstion;
            }
            else
            {
                filename = path + Application.loadedLevelName + JsonKey.JsonExtenstion;
            }
            return filename;
        }
        public static void SaveJsonFile(Dictionary<string, Dictionary<string,object>> dict, bool append, bool IsSettings)
        {
            string filename =SavingJsonFile.ConstructFileName(IsSettings);
            
            using (StreamWriter sw = new StreamWriter(filename, append))
            {
                string str = Json.Serialize(dict);
                Debug.Log("Serelized:" + str);
                sw.Write(str);
                Debug.Log("saving file: " + filename);
            }
        }

        public static void SaveJsonFile(Dictionary<string, object> dict, bool append, bool IsSettings)
        {
            string filename = SavingJsonFile.ConstructFileName(IsSettings);

            using (StreamWriter sw = new StreamWriter(filename, append))
            {
                string str = Json.Serialize(dict);
                Debug.Log("Serelized:" + str);
                sw.Write(str);
                Debug.Log("saving file: " + filename);
            }
        }
        public static Dictionary<string, object> LoadJsonFile()
        {
            string[] lines = System.IO.File.ReadAllLines(path);
            string content= lines[JsonKey.Level];
            //string line = string.Empty;
            //int lineNumber=0;
            //string content = "";
            //using (StreamReader sr = new StreamReader(Application.dataPath + "/" + JsonKey.FileName))
            //{
            //    while (((line = sr.ReadLine()) != null))
            //    {
            //        Debug.Log("saving level num: " + JsonKey.Level);
            //        if (lineNumber == JsonKey.Level)
            //        {
            //            content = sr.ReadLine();
            //            break;
            //        }
            //        else lineNumber++;
            //    }
            //    Debug.Log("serialised SavingJsonFileClass: "+content);
            //}
            Debug.Log("serialised SavingJsonFileClass: " + content);
            var dict = (Dictionary<string, object>)Json.Deserialize(content) as Dictionary<string, object>;
            return dict;

        }

        public static Dictionary<string, object> LoadJsonFile(bool IsSettings)
        {
            string filename = SavingJsonFile.ConstructFileName(IsSettings);
            if (!File.Exists(filename))
            {
                Debug.Log("file does not exist!!");
                return null;
            }
            string content = "";
            using (StreamReader sr = new StreamReader(filename))
            {
                content = sr.ReadLine();
                Debug.Log("loading file: " + filename);
            }
            Debug.Log("serialised SavingJsonFileClass: " + content);
            var dict = (Dictionary<string, object>)Json.Deserialize(content) as Dictionary<string, object>;
            return dict;
        }

        public static Dictionary<string, object> SerelizeMysteriousMan(List<object> l)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            dict.Add(JsonKey.MysteriousManTag, l);
            return dict;
        }

        public static Dictionary<string, object> SerelizeLevel(Dictionary<string,object> l, int index)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            dict.Add(JsonKey.LevelTag + index, l);
            return dict;
        }

        public static Dictionary<string, object> serelizePositionAndTime(Vector3 postion, float time, int index)
        {
            List<object> l = AddToList(postion.x, postion.y, postion.z);
            Dictionary<string, object> dict = new Dictionary<string, object>();
            dict.Add(JsonKey.PostionTag + index, l);
            dict.Add(JsonKey.TimeTag + index, time);
            return dict;
        }
        public static List<object> AddToList(params float[] list)
        {
            List<object> l = new List<object>();

            for (int i = 0; i < list.Length; i++)
            {
                l.Add(list[i]);
            }
            return l;
        }
       
    }
}
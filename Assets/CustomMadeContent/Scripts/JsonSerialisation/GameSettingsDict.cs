﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace OrangeSummerChallenge.JsonSerialisation
{
    public static class GameSettingsDict
    {

        private static Dictionary<string, Dictionary<string, object>> mainDict;

        public static Dictionary<string, Dictionary<string, object>> SettingsDictionary
        {
            get { return mainDict; }
            set { mainDict = value; }
        }
        private static Dictionary<string, object> dict;

        public static void InitSettingsDict()
        {
            mainDict = new Dictionary<string, Dictionary<string, object>>();
            Dictionary<string, object> optionDict = new Dictionary<string, object>();
            optionDict.Add(JsonKey.VRTag, GameOption.VR);
            optionDict.Add(JsonKey.SoundEffectsTag, GameOption.SoundEffects);
            optionDict.Add(JsonKey.VerticalSensitivityTag, GameOption.VerticalSensitivity);
            optionDict.Add(JsonKey.HorizentalSensitivityTag, GameOption.HorizentalSensitivity);
            optionDict.Add(JsonKey.BackgroundMusicTag, GameOption.BackgroundMusic);
            optionDict.Add(JsonKey.LevelTag, GameOption.Level);
            mainDict.Add(JsonKey.SettingsTag, optionDict);
        }

        public static Dictionary<string, Dictionary<string, object>> ModifySettings(params KeyValuePair<string, object>[] SettingsOption)
        {
            for (int i = 0; i < SettingsOption.Length; i++)
            {
                Debug.Log("GameSettings KEY: "+SettingsOption[i].Key);
                if (mainDict[JsonKey.SettingsTag].ContainsKey(SettingsOption[i].Key))
                {
                    mainDict[JsonKey.SettingsTag][SettingsOption[i].Key] = SettingsOption[i].Value;
                }
                else
                {
                    Debug.Log("JSON KEY: key does not exist");
                }
                dict = mainDict[JsonKey.SettingsTag];
            }
            return mainDict;
        }
        public static void Commit()
        {
            SavingJsonFile.SaveJsonFile(mainDict, false, true);
            UpdateGameSettings();
        }
        public static void LoadGameSettings()
        {
            dict = SavingJsonFile.LoadJsonFile(true)[JsonKey.SettingsTag] as Dictionary<string, object>;
            UpdateGameSettings();
        }
        public static void UpdateGameSettings()
        {

            GameOption.VR = (bool)dict[JsonKey.VRTag];
            Debug.Log("VR: "+GameOption.VR);
            GameOption.BackgroundMusic = Convert.ToSingle(dict[JsonKey.BackgroundMusicTag]);
            GameOption.HorizentalSensitivity = Convert.ToSingle(dict[JsonKey.HorizentalSensitivityTag]);
            GameOption.SoundEffects = Convert.ToSingle(dict[JsonKey.SoundEffectsTag]);
            GameOption.VerticalSensitivity = Convert.ToSingle(dict[JsonKey.VerticalSensitivityTag]);
            GameOption.Level = Convert.ToInt32( dict[JsonKey.LevelTag]);
        }

    } 
}
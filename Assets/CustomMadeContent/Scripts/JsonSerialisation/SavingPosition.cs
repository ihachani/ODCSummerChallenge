﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace OrangeSummerChallenge.JsonSerialisation
{
    public class SavingPosition : MonoBehaviour
    {

        bool saved;
        private float LastInterval;
        private float timeNow;

        public float TimeNow
        {
            get { return timeNow; }
            set { timeNow = value; }
        }
        [SerializeField]
        private float TimeInterval;
        private List<object> Positionlist;
        private Dictionary<string, object> PositionDict;
        private Dictionary<string, object> MainPositionDict;
        private Dictionary<string, object> LevelDict;
        private int nbrPosition;

        void Start()
        {
            InitialiazeVariables();
        }
        /// <summary>
        /// Variable Initialiazation.
        /// </summary>
        private void InitialiazeVariables()
        {
            saved = false;
            LastInterval = Time.timeSinceLevelLoad;
            Positionlist = new List<object>();
            PositionDict = new Dictionary<string, object>();
            MainPositionDict = new Dictionary<string, object>();
            LevelDict = new Dictionary<string, object>();
            nbrPosition = 0;
        }
        /// <summary>
        /// this function is called by the script which load the next scene
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> PrepareDictionnaryToSave()
        {
            LevelDict.Add(Application.loadedLevelName, MainPositionDict);
            return LevelDict;
        }
        void Update()
        {
            timeNow = Time.timeSinceLevelLoad;
            if (timeNow > LastInterval + TimeInterval)
            {
                PositionDict = SavingJsonFile.serelizePositionAndTime(this.transform.position, timeNow, nbrPosition);
                MainPositionDict.Add(JsonKey.MainPostionTag + nbrPosition, PositionDict);
                nbrPosition++;
                LastInterval = timeNow;

            }
            this.transform.Translate(Vector3.forward * Time.deltaTime);
        }
    }
    
}
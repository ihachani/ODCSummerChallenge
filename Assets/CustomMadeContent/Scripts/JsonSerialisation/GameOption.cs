﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace OrangeSummerChallenge.JsonSerialisation
{
    [System.Serializable]
    public static class GameOption
    {
        [SerializeField]
        private static bool vr;
        
        [SerializeField]
        private static int level;

        public static int Level
        {
            get { return GameOption.level; }
            set { GameOption.level = value; }
        }

        [SerializeField]
        [Range(0f, 100f)]
        private static float soundEffects;

        [SerializeField]
        [Range(0f, 100f)]
        private static float backgroundMusic;

        [SerializeField]
        [Range(0f, 2.5f)]
        private static float verticalSensitivity;

        [SerializeField]
        [Range(0f, 2.5f)]
        private static float horizentalSensitivity;

        public static bool VR
        {
            get { return vr; }
            set { vr = value; }
        }

        public static float SoundEffects
        {
            get { return soundEffects; }
            set { soundEffects = value; }
        }
        public static float BackgroundMusic
        {
            get { return backgroundMusic; }
            set { backgroundMusic = value; }
        }
        public static float VerticalSensitivity
        {
            get { return verticalSensitivity; }
            set { verticalSensitivity = value; }
        }
        public static float HorizentalSensitivity
        {
            get { return horizentalSensitivity; }
            set { horizentalSensitivity = value; }
        }
    }
}


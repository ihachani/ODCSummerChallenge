﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace OrangeSummerChallenge.Automatic_Behaviour
{
    class ObjectAvoidance  : MonoBehaviour
    {
        private bool solCollision = false;
        private bool notify = false;

        private Vector3 forceVector;
        private Vector3 antiForce;
        private Vector3 forceAnnulator;
        private int rate;
        private float timeToWait;


        private Rigidbody rgd;



        private static int GetLayerMask()
        {
            int mask = LayerMask.NameToLayer("DesertTerrain");
            return mask;
        }



        public void NotifyObject(Vector3 force , Vector3 anti , Vector3 nullator , int rat , float tim) {
            this.forceVector = force;
            this.antiForce = anti;
            this.forceAnnulator = nullator;

            this.rate = rat;
            this.timeToWait = tim;
            Debug.Log("List Of Interaction for Avoidance Object Init...");


            solCollision = false;
            StartCoroutine("Wait");
        }


        IEnumerator Wait()
        {
            rgd.AddForce(forceVector * rate);
            Debug.Log("Force Added : " + forceVector.x + " Y = " + forceVector.y);
            yield return new WaitForSeconds(timeToWait);
            rgd.AddForce(antiForce * rate);
            if (forceVector.y != 0)
            {
                Debug.Log("Loop Entred....");
                solCollision = false;
                notify = true;
            }

            else {
                Debug.Log("Not an Attraction Force");
                yield return new WaitForSeconds(timeToWait);
                rgd.AddForce(forceAnnulator * rate);
                yield return null;
            }

        }


        


        void Start() { 

            rgd= this.GetComponent<Rigidbody>() ;
        
        }



        void Update()
        {
            if (!solCollision && notify)
            {
                RaycastHit rayHit;
                int mask = GetLayerMask();
                Vector3 rayEmit = new Vector3(this.transform.position.x, this.transform.position.y + 1f, this.transform.position.z);
                var hit = Physics.Raycast(rayEmit, this.transform.up * (-1), out rayHit, 5f, mask);
                //Debug.Log("Value of hit : " + hit);
                if (hit)
                {
                    if (rayHit.distance < 3)
                    {

                        rgd.AddForce(forceAnnulator * rate);
                        solCollision = true;
                        notify = false;
                        Debug.Log("Collision Detected");

                    }

                }



            }



        }


    }

}

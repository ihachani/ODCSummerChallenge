﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace OrangeSummerChallenge.Automatic_Behaviour
{
    class ObstacleAvoidance : MonoBehaviour
    {



        #region booleans

        bool solCollision=false;
        bool notify=false;


        #endregion



        [SerializeField]
        private Vector3 forceVector;

        [SerializeField]
        private Vector3 antiForce;

        [SerializeField]
        private Vector3 forceAnnulator;

        [SerializeField]
        private int rate;

        [SerializeField]
        private float timeToWait;

       


        void OnTriggerEnter(Collider other)
        {


            Debug.Log("Entred...");
            if (other.tag == "target")
            {
               ObjectAvoidance  observer = other.gameObject.GetComponent<ObjectAvoidance>();
               observer.NotifyObject(forceVector,antiForce,forceAnnulator,rate, timeToWait);
                

            }

            else
            {

                Debug.Log("Has No Automatic behaviour");

            }




        }

    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MyProgressBarScript : MonoBehaviour {
    [SerializeField]
    Image CircleImage;
    [SerializeField]
    Color start;
    [SerializeField]
    Color end;
    [SerializeField]
    float focusTime;
    [SerializeField]
    Color current;

    // Use this for initialization
    void Start()
    {
        CircleImage.type = Image.Type.Filled;
        CircleImage.fillMethod = Image.FillMethod.Radial360;
        CircleImage.fillOrigin = 0;
    }

    // Update is called once per frame
    void Update()
    {
        for (float i = 0; i < 1; i += 0.1f)
        {
            CircleImage.fillAmount = Mathf.Max(i, 0.001f);
            Debug.Log(i);
        }

    }
}

﻿using UnityEngine;
using System.Collections;

namespace OrangeSummerChallenge.Gyro
{
    public class GyroRotate : MonoBehaviour
    {
        [SerializeField]
        private GyroInitializer gyroInitializer;
        private Gyroscope gyro;

        private float initialOrientationX;
        private float initialOrientationY;
        private float initialOrientationZ;
        void Start()
        {
            if (SystemInfo.supportsGyroscope)
            {
                Vector3 intialRotation = gyroInitializer.Init();

                intialRotation = GetInitialInput(intialRotation);
            }
        }

        private Vector3 GetInitialInput(Vector3 intialRotation)
        {
            initialOrientationX = intialRotation.x;
            initialOrientationY = intialRotation.y;
            initialOrientationZ = intialRotation.z;
            return intialRotation;
        }
        void FixedUpdate()
        {
            RotateObjectUsingGyro();
        }

        private void RotateObjectUsingGyro()
        {
            transform.Rotate(CalculateXRotation(), CalculateYRotation(), CalculateZRotation());
        }

        private float CalculateZRotation()
        {
            return initialOrientationZ + Input.gyro.rotationRateUnbiased.z;
        }

        private float CalculateYRotation()
        {
            return initialOrientationY - Input.gyro.rotationRateUnbiased.y;
        }

        private float CalculateXRotation()
        {
            return initialOrientationX - Input.gyro.rotationRateUnbiased.x;
        }

    }

}
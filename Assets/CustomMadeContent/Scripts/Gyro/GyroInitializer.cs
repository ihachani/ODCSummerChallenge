﻿using UnityEngine;
using System.Collections;
using System;

namespace OrangeSummerChallenge.Gyro
{
    [Serializable]
    public class GyroInitializer 
    {
        private Gyroscope gyro;
        public Vector3 Init()
        {
            gyro = Input.gyro;
            gyro.enabled = true;
            float initialOrientationX = Input.gyro.rotationRateUnbiased.x;
            float initialOrientationY = Input.gyro.rotationRateUnbiased.y;
            float initialOrientationZ = -Input.gyro.rotationRateUnbiased.z;
            return new Vector3(initialOrientationX, initialOrientationY, initialOrientationZ);
        }
    }
    
}
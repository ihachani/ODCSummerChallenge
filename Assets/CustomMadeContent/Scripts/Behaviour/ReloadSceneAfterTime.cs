﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace OrangeSummerChallenge.Behaviour
{
    class ReloadSceneAfterTime : MonoBehaviour
    {
        
        [SerializeField]
        private AudioSource source;
        [SerializeField]
        private String NameSceneToLoad ;

        void OnTriggerEnter(Collider other) {

            if (other.tag == "Player")
            {
                StartCoroutine("TelepoterToScene") ;
             
            }

            else {

                Debug.Log("Somthing But not Player Entred");
            
            }
        }


        IEnumerator TelepoterToScene() {
        
            yield return new WaitForSeconds(source.clip.length);
            Application.LoadLevel(NameSceneToLoad) ;
            yield return null ;
        
        }



    }

    }


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace OrangeSummerChallenge.Behaviour
{
    class CollisionSound : MonoBehaviour
    {

        [SerializeField]
        private AudioSource source;

        void OnTriggerEnter(Collider other) {

            if (other.tag == "Player")
            {

                source.Play();
            }

            else {

                Debug.Log("Somthing But not Player Entred");
            
            }

        
        }
    }
}

﻿using OrangeSummerChallenge.Actions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace OrangeSummerChallenge.Behaviour
{
    /// <summary>
    /// When an object enter in the trigger
    /// the method of Lose Sahara Action 
    /// is Called
    /// </summary>
    class EnterCollisionObstacle : MonoBehaviour
    {
        [SerializeField]
        private LoseSaharaAction action;



        void OnTriggerEnter(Collider other)
        {

            Debug.Log("Success : Something Entreed");
            if (other.tag == "Player") {
                Debug.Log("Success : Player Entred in Collision With This Obstacle");
                action.Action();
            }

        
        }

    }
}

﻿using OrangeSummerChallenge.Actions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace OrangeSummerChallenge.Behaviour
{
    class AddForceBehavioursAfterSound : MonoBehaviour 
    {

        [SerializeField]
        private AudioSource src;

        private bool AudioEnded = false ;

        public AttractionForce att;
        void FixedUpdate()
        {
            if (AudioEnded)
            {
                att.Action();
            }
        }

        void Start() {

            StartCoroutine(WaitAudio());
        
        }


        IEnumerator WaitAudio() {
            yield return new WaitForSeconds(src.clip.length+ 0.5f);
            AudioEnded = true;
            yield return null;

        
        }

    }
}

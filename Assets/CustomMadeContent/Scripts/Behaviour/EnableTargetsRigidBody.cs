﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;
namespace OrangeSummerChallenge.Behaviour
{
    class EnableTargetsRigidBody : MonoBehaviour
    {
        [SerializeField]
        private AudioSource play;

        [SerializeField]
        private List<GameObject> list;

        [SerializeField]
        private Rigidbody body;


        private bool test = false;


        void OnTriggerExit(Collider col)
        {
            test = true;
        }


        void OnTriggerEnter(Collider other)
        {

            if (other.tag == "Player")
            {

                Debug.Log("Player Entred");
                if (!test)
                {
                    play.Play();
                    StartCoroutine(playAudioandEnable());
                    test = true;
                }

            }


        }


        IEnumerator playAudioandEnable()
        {
            yield return new WaitForSeconds(play.clip.length);
            for (int i = 0; i < list.Count; i++)
            {

                list[i].SetActive(true);

            }

           
            

        }
    }
}

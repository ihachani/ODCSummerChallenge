﻿using UnityEngine;
using System.Collections;
using OrangeSummerChallenge.Utility;
namespace OrangeSummerChallenge.Managers
{
    public class SceneLoadManager : MonoBehaviour
    {
        [SerializeField]
        private LoadAsync asyncLoader;
        /// <summary>
        /// Used To StartLoading The Scene on background.
        /// </summary>
        public void LoadNextScene()
        {
           // asyncLoader.LoadNextLevel();
            int level = Application.loadedLevel;
            if (level < Application.levelCount)
            {
                Application.LoadLevel(level + 1);
            }
            else 
            {
                Debug.Log("lastScene");
                Application.LoadLevel(0);
            }
        }
        public void LoadScene(int number)
        {
            //asyncLoader.LoadScene(number);
            Application.LoadLevel(number);
        }
        public void ReloadScene(){
            //asyncLoader.LoadScene(CurrentLevel());
            Application.LoadLevel(CurrentLevel());
        }
        private int CurrentLevel()
        {
            return Application.loadedLevel;
        }
    }
    
}
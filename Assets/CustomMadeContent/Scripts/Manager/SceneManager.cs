﻿using OrangeSummerChallenge.JsonSerialisation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace OrangeSummerChallenge.Manager
{
    class SceneManager : MonoBehaviour
    {
        [SerializeField]
        bool FollowingBehaviour;
        [SerializeField]
        private SavingPosition savingPosition;
        [SerializeField]
        private Managers.SceneLoadManager sceneLoadManager;
        public void LoadNextScene() 
        {
            if (FollowingBehaviour)
            {
                Dictionary<string, object> dict = savingPosition.PrepareDictionnaryToSave();
                SavingJsonFile.SaveJsonFile(dict, false, false);
            }
            sceneLoadManager.LoadNextScene();
        }

        public void ReloadScene() 
        {
            sceneLoadManager.ReloadScene();
        }



        public void LoadLastPlayedScene()
        {
            GameSettingsDict.LoadGameSettings();
            sceneLoadManager.LoadScene(GameOption.Level);
            //Application.LoadLevel(GameOption.Level);


        }

    }
}

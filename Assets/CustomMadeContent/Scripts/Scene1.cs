﻿using UnityEngine;
using System.Collections;
using OrangeSummerChallenge.Navigation;

//TODO Reuse this.
public class Scene1 : MonoBehaviour {


    private Collider oth;
    
    [SerializeField]private GameObject target2; 


	// Use this for initialization
	void Start () {

     
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other) {
        Debug.Log("Other Tag :"+other.tag);
        oth = other;
        if (other.tag == "Player") {
            StartCoroutine("Deplacer");
            
        
        }
    }

    IEnumerator Deplacer() {
        yield return new WaitForSeconds(5f);
        this.transform.position = target2.transform.position;
        yield return new WaitForSeconds(2f);
        oth.GetComponent<SimpleNavigationBehavior>().NavigationManger.SetDestination(target2.transform.position); 


    
    }
    
    
    }

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 

namespace OrangeSummerChallenge.TriggersInterActions.Scene1
{
    public class FocusDesactivator : MonoBehaviour
    {
        [SerializeField]
        private List<GameObject> objectToDesactivateFocus;


        void OnTriggetEnter(Collider other) { 
        
            if(other.tag=="Player") {

                for (int i = 0; i < objectToDesactivateFocus.Count; i++) {

                    objectToDesactivateFocus[i].layer = 0;
                }
            }
        
        }
        // Use this for initialization

    }

}

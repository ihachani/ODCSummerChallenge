﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 


namespace OrangeSummerChallenge.TriggersInterActions.Scene1
{
    public class FocusActivation : MonoBehaviour
    {
        [SerializeField]
        private List<GameObject> ObjectToFocus;

        void OnTriggerEnter(Collider other) {

            if (other.tag == "Player")
            {
                for (int i = 0; i < ObjectToFocus.Count; i++ )
                {
                    ObjectToFocus[i].layer = 8;
                    Debug.Log("Object "+i+" set to Focus");
                }

            }

        
        }
    }
}

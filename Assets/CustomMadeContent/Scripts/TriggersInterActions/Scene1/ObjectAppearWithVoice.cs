﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace OrangeSummerChallenge.TriggersInterActions.Scene1
{
    public class ObjectAppearWithVoice : MonoBehaviour
    {
        [SerializeField]
        List<GameObject> ObjectToAppear;
        [SerializeField]
        AudioSource SoundToPlay;
        Animator anim;

        // Use this for initialization
        void Start()
        {       
            

        }

        // Update is called once per frame
        void Update()
        {


        }

        void OnTriggerEnter(Collider oth) {
           
            if (oth.tag == "Player") {
                Debug.Log("Here I am a Player");
                StartCoroutine("AppearWithSound");
            
            }

        }


        IEnumerator AppearWithSound() {
            yield return new WaitForSeconds(1f);

            for (int i = 0; i < ObjectToAppear.Count; i++)
            {
                anim = ObjectToAppear[i].GetComponent<Animator>(); 
                anim.SetTrigger("Apprear");

            }
            SoundToPlay.Play();
            yield return new WaitForSeconds(1f);

            Debug.Log("Animation Played");
        } 



    }
}
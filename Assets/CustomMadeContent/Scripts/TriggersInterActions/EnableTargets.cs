﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace OrangeSummerChallenge.TriggersInterActions
{
    public class EnableTargets : MonoBehaviour
    {
        [SerializeField]
        private List<GameObject> gameObjectsToEnable;
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        void OnTriggerEnter(Collider other)
        {
            Debug.Log("Entred");
            if (other.tag == "Player")
            {
                foreach (var gameObject in gameObjectsToEnable)
                {
                    gameObject.SetActive(true);
                }
            }
        }
    }
    
}
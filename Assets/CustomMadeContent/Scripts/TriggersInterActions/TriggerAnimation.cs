﻿using UnityEngine;
using System.Collections;

namespace OrangeSummerChallenge.TriggersInterActions
{
    public class TriggerAnimation : MonoBehaviour
    {
        [SerializeField]
        private Animator targetAnimator;
        [SerializeField]
        private string triggerName;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Player")
            {
                targetAnimator.SetTrigger(triggerName);
            }
        }
    }
    
}
﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace OrangeSummerChallenge.TriggersInterActions
{
    public class DisableTargets : MonoBehaviour
    {
        [SerializeField]
        private List<GameObject> gameObjectsToDisable;
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Player")
            {
                for (int i = 0; i < gameObjectsToDisable.Count; i++)
                {
                    var gameObject = gameObjectsToDisable[i];
                    gameObjectsToDisable.RemoveAt(i);
                    gameObject.SetActive(false);
                }
            }
        }
    }

}
﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using OrangeSummerChallenge.Focus.Actions.MovingToTarget;
using OrangeSummerChallenge.Focus;

namespace OrangeSummerChallenge.TriggersInterActions
{

    public class UnlockTargets : MonoBehaviour
    {

        [SerializeField]
        private List<MonoBehaviour> targets;

        // Use this for initialization
        void Awake()
        {
            for (int i = 0; i < targets.Count; i++)
            {
                targets[i].enabled = false;
            }
        }

        // Update is called once per frame
        void Update()
        {

        }

        void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Player")
            {
                for (int i = 0; i < targets.Count; i++)
                {
                    targets[i].enabled = true;
                }


            }

        }
    }

}

﻿using UnityEngine;
using System.Collections;

namespace OrangeSummerChallenge.TriggersInterActions
{
    public class ToggleCollider : MonoBehaviour
    {
        [SerializeField]
        private Collider colliderToToggle;
        void OnTriggerEnter(Collider other)
        {
            colliderToToggle.enabled = false;
        }
        void OnTriggerExit(Collider other)
        {
            colliderToToggle.enabled = true;
        }
    }
    
}
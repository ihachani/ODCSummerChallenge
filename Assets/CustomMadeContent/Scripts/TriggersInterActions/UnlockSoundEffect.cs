﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using OrangeSummerChallenge.Focus.Actions.SoundActivation;


namespace OrangeSummerChallenge.TriggersInterActions
{

    public class UnlockSoundEffect : MonoBehaviour
    {

        [SerializeField]
        private List<GameObject> targetsWithSound;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        void OnTriggerEnter(Collider other)
        {
            Debug.Log("Entred");
            if (other.tag == "Player")
            {

                for (int i = 0; i < targetsWithSound.Count; i++)
                {

                    targetsWithSound[i].GetComponent<SoundBehaviour>().enabled = true;

                }


            }

        }
    }

}

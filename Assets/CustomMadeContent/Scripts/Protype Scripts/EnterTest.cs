﻿using UnityEngine;
using System.Collections;

public class EnterTest : MonoBehaviour {

     /*<Summary>
      * This Script Is Responsible 
      * For the Desctructin of Scene 2 
      * 
      * 
      * Designed for This Level Exactely
      * 
      * 
      */




    private  GameObject[] list;  //List Of Element To Destruct
   


	// Use this for initialization
	void Start () {

        list = GameObject.FindGameObjectsWithTag("Row0");
       
	}
	

  //When a Player Enter
    void OnTriggerEnter(Collider other)
    {


        if (other.tag == "Player")
        {
            StartCoroutine("destructionCouroutine" , list) ;
        }
    }



// Destruction Couroutine
 public IEnumerator destructionCouroutine(GameObject[] list ) {
    for (int i = 0; i < list.Length; i++)
        {
            Animator anim = list[i].GetComponent<Animator>();
            anim.SetTrigger("Fall");
            yield return new WaitForSeconds(0.5f);
            list[i].SetActive(false);
            
        }
    float rnd = Random.Range(0.00f, 1.00f);
        yield return new WaitForSeconds(0.2f * rnd ); 
    }




    }


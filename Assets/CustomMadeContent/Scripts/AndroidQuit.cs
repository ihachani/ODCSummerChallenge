﻿using UnityEngine;
using System.Collections;

public class AndroidQuit : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        QuitApplication();
    }
    /// <summary>
    /// Listens for Escape Key And quits the application.
    /// </summary>
    private static void QuitApplication()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) { Application.Quit(); }
    }
}

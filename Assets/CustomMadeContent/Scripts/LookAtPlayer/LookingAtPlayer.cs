﻿using UnityEngine;
using System.Collections;

public class LookingAtPlayer : MonoBehaviour {
    [SerializeField]
    private Transform Player;
	
	// Update is called once per frame
	void Update () {
        this.transform.LookAt(Player);
	}
}

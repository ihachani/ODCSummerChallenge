﻿using UnityEngine;
using System.Collections;

public class GamePlay : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Game.current=new Game();
		Game.current.config.VR = true;
		SaveLoad.Save();
		SaveLoad.Load();
		foreach(Game g in SaveLoad.savedGames)
		{
			Debug.Log(g.config.VR);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Configuration {

	[SerializeField]
	private bool vr;
	
	[SerializeField]
	[Range(0f, 100f)]
	private float soundEffects;
	
	[SerializeField]
	[Range(0f, 100f)]
	private float backgroundMusic;
	
	[SerializeField]
	[Range(0f, 2.5f)]
	private float verticalSensitivity;
	
	[SerializeField]
	[Range(0f, 2.5f)]
	private float horizentalSensitivity;

	public bool VR
	{
		get { return vr; }
		set { vr = value; }
	}

	public float SoundEffects
	{
		get { return soundEffects; }
		set { soundEffects = value; }
	}
	public float BackgroundMusic
	{
		get { return backgroundMusic; }
		set { backgroundMusic = value; }
	}
	public float VerticalSensitivity
	{
		get { return verticalSensitivity; }
		set { verticalSensitivity = value; }
	}
	public float HorizentalSensitivity
	{
		get { return horizentalSensitivity; }
		set { horizentalSensitivity = value; }
	}
}

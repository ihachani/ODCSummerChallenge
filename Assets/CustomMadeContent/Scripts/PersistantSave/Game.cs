﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Game {

	public static Game current;
	public Configuration config;
	
	public Game () {
		config = new Configuration();
	}
}

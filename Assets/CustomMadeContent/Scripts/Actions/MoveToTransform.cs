﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine; 
namespace OrangeSummerChallenge.Actions
{
    [Serializable]
    class MoveToTransform : IAction
    {
        [SerializeField]
        private Transform Target;

        [SerializeField]
        private NavMeshAgent AgentToMove;



        public void Action()
        {
            AgentToMove.SetDestination(Target.position);
        }
    }
}

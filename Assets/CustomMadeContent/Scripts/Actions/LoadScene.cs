﻿using OrangeSummerChallenge.JsonSerialisation;
using OrangeSummerChallenge.Manager;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace OrangeSummerChallenge.Actions
{
    [Serializable]
    public class LoadSceneAction : IAction
    {

        [SerializeField]
        private SceneManager mng; 
        [SerializeField]
        private float timeToWait;
        [SerializeField]
        private MonoBehaviour couroutineStarter;
        public void Action()
        {
            couroutineStarter.StartCoroutine(waitToChangeScene());
        }
        private IEnumerator waitToChangeScene()
        {
            
            yield return new WaitForSeconds(timeToWait);
            mng.LoadNextScene();
        }
    }
}

﻿using System;
namespace OrangeSummerChallenge.Actions
{
    interface IAction
    {
        void Action();
    }
}

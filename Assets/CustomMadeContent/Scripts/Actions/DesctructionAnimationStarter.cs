﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
namespace OrangeSummerChallenge.Actions
{

    [Serializable]
    public class DestructionAnimationStarter : IAction
    {
        [SerializeField]
        private List<Animator> AnimatorToPlay;
        [SerializeField]
        private String AnimationName;

        public void Action()
        {
            for (int i = 0; i < AnimatorToPlay.Count; i++)
            {
                AnimatorToPlay[i].SetTrigger(AnimationName);
            }
        }
    }
}

﻿using OrangeSummerChallenge.Actions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;




namespace OrangeSummerChallenge.Actions
{
    /// <summary>
    /// When You Enter in The Collider of an Obstacle
    /// it use this Action to display a sound 
    /// and make you return to your first 
    /// </summary>

    [Serializable]
    class LoseSaharaAction : IAction 
    {
        [SerializeField]
        private Transform mesh;
        [SerializeField]
        private Transform initPosition;
        [SerializeField]
      //  private AudioSource soundToLoose;  //Sound To Display When Loose 


        public void Action()
        {
           // soundToLoose.Play();
            mesh.position = initPosition.position;
            Debug.Log("Success SAHARA 0101 : You Lost Game");
        }
    }
}

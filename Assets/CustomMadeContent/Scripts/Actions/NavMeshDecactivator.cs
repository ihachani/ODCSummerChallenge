﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace OrangeSummerChallenge.Actions
{
    [Serializable]
    class NavMeshDecactivator : IAction
    {
        [SerializeField]
        private NavMeshAgent navAgentToDisable;

        public void Action()
        {
            navAgentToDisable.enabled = false;
            
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using System;

namespace OrangeSummerChallenge.Actions{
    [Serializable]
    public class AttractionForce : IAction{
        [SerializeField]
        private Rigidbody obj;
        [SerializeField]
        private float InitSpeed;
        [SerializeField]
        private float IncrementRate;
        [SerializeField]
        private float LimitSpeed;
        [SerializeField]
        private Vector3 forceVector;


        public void setNewDirection(Vector3 vect, bool left) {

            Vector3 interForce = new Vector3(forceVector.x * (-1), forceVector.y * (-1), forceVector.z * (-1));

            
            obj.velocity = Vector3.zero;
            
            
            Debug.Log("Forces Annulated");
            forceVector = vect;
            if (left) {

               // obj.MoveRotation(Quaternion.Angle() );
            
            }
            
        
        }



        public void Action()
        {
            obj.AddForce(forceVector * InitSpeed);
            if (InitSpeed < LimitSpeed)
            {
                InitSpeed += IncrementRate;
            }
        }
    }
}
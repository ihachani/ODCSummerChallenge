﻿using UnityEngine;
using System.Collections;
using OrangeSummerChallenge.Manager;

public class LoadSceneAfterTime : MonoBehaviour
{
    [SerializeField]
    private SceneManager mng;
    [SerializeField]
    private float timeToWait;
    private float elapsedTime = 0f;
    private bool finishedWaiting;
    // Use this for initialization
    void Start()
    {
        finishedWaiting = false;
    }

    // Update is called once per frame
    void Update()
    {
        elapsedTime += Time.deltaTime;
        if (elapsedTime >= timeToWait && !finishedWaiting)
        {
            finishedWaiting = true;
            mng.LoadNextScene();
        }
    }
}

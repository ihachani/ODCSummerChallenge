﻿using UnityEngine;
using System.Collections;
using OrangeSummerChallenge.Focus;

namespace OrangeSummerChallenge
{
    /// <summary>
    /// Test Mono used to show focus events.
    /// </summary>
    public class ColorChangeOnFocusEvents : MonoBehaviour
    {
        Renderer rend;
        IFocusableBehavior focusable;
        // Use this for initialization
        void Start()
        {
            focusable = GetComponent<FocusableBehaviour>();

            AddFocusEventHandlers();

        }

        private void AddFocusEventHandlers()
        {
            focusable.ObjectFocused += focusable_ObjectFocused;
            focusable.ObjectFocusComplete += focusable_ObjectFocusComplete;
            focusable.ObjectFocusLost += focusable_ObjectFocusLost;
        }
        void OnEnable()
        {
            rend = GetComponent<Renderer>();
        }

        void focusable_ObjectFocusLost(object sender, FocusEventArgs eventArgs)
        {
            SetColorToWhite();
        }

        void focusable_ObjectFocusComplete(object sender, FocusEventArgs eventArgs)
        {

            SetColorToBlue();
        }

        void focusable_ObjectFocused(object sender, FocusEventArgs eventArgs)
        {
            SetColorToRed();
        }
        // Update is called once per frame
        void Update()
        {

        }

        private void SetColorToRed()
        {
            rend.material.SetColor("_Color", Color.red);
        }

        private void SetColorToBlue()
        {
            rend.material.SetColor("_Color", Color.blue);
        }
        private void SetColorToWhite()
        {
            rend.material.SetColor("_Color", Color.white);
        }
    }

}
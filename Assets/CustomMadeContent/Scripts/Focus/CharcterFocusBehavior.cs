﻿using UnityEngine;
using System.Collections;

namespace OrangeSummerChallenge.Focus
{
    public class CharcterFocusBehavior : MonoBehaviour
    {
        [SerializeField]
        const string focusableLayerName = "Focusable";
        #region Private Fields
        private IFocusableBehavior lastFocusedObject = null;
        private LayerMask mask;
        #endregion
        // Use this for initialization
        #region UnityLifeCycleMethods
        void Start()
        {
            mask = CreateLayerMask();
        }
        void FixedUpdate()
        {

            FocusableObjectDetection();
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Create A layerMask for the focusable Layer.
        /// </summary>
        /// <returns></returns>
        private LayerMask CreateLayerMask()
        {
            LayerMask mask = LayerMask.GetMask("Focusable");
            return mask;
        }

        private void FocusableObjectDetection()
        {

            RaycastHit hit;
            hit = new RaycastHit();
            if (Raycast(ref hit, ref mask))
            {
               // Debug.Log(hit.distance + " away");

                FocusableFoundRoutine(hit);
            }
            else if (lastFocusedObject != null)
            {
                UnsetLastFocusedObject();
            }
        }

        /// <summary>
        /// Raycast using Focusable as LayerMask.
        /// </summary>
        /// <param name="hit">Hit object to retrieve results.</param>
        /// <param name="mask">Focusable LayerMask</param>
        /// <returns></returns>
        private bool Raycast(ref RaycastHit hit, ref LayerMask mask)
        {
            return Physics.Raycast(gameObject.transform.position, gameObject.transform.forward, out hit, 30f, mask);
        }

        /// <summary>
        /// Focusabel Found Routine.
        /// </summary>
        /// <param name="hit"></param>
        private void FocusableFoundRoutine(RaycastHit hit)
        {
          //  Debug.Log("FaceShit: " + hit.collider.gameObject.name);
            IFocusableBehavior focusableBehavior = GetFocusableBehaviorFromHit(ref hit);
            UpdateLastFocusedObject(focusableBehavior);
            //Debug.Log("Focusable: " + focusableBehavior);
            focusableBehavior.Focused();
        }

        /// <summary>
        /// Update The lastFocusedObject. Calls FocusLost before changing the value.
        /// </summary>
        /// <param name="focusableBehavior">The new found object.</param>
        private void UpdateLastFocusedObject(IFocusableBehavior focusableBehavior)
        {
            if (lastFocusedObject == null)
            {
                lastFocusedObject = focusableBehavior;
            }
            else if (lastFocusedObject != focusableBehavior)
            {
                lastFocusedObject.FocusLost();
                lastFocusedObject = focusableBehavior;
            }
        }
        /// <summary>
        /// Returns the IFocusableBehavior from the hit.
        /// </summary>
        /// <param name="hit"></param>
        /// <returns></returns>
        private IFocusableBehavior GetFocusableBehaviorFromHit(ref RaycastHit hit)
        {
            GameObject hitObject = hit.collider.gameObject;
            IFocusableBehavior focusableBehavior = hitObject.GetComponent<FocusableBehaviour>();
            return focusableBehavior;
        }
        /// <summary>
        /// Uset lastFocusedObject. Calls FocusLost before changing the value.
        /// </summary>
        private void UnsetLastFocusedObject()
        {
            lastFocusedObject.FocusLost();
            lastFocusedObject = null;
        }

        #endregion

    }

}
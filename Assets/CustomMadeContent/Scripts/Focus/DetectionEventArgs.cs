﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OrangeSummerChallenge.Focus
{
    public class DetectionEventArgs : EventArgs
    {
        IFocusableBehavior focusable;

        public IFocusableBehavior Focusable
        {
            get { return focusable; }
            set { focusable = value; }
        }

    }
}

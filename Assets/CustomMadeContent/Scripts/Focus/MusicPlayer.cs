﻿using UnityEngine;
using System.Collections;
using OrangeSummerChallenge.Focus;

namespace OrangeSummerChallenge
{
    /// <summary>
    /// Test Mono used to show focus events.
    /// </summary>
    public class MusicPlayer : MonoBehaviour
    {



        //Local Variables 
        Renderer rend;
        IFocusableBehavior focusable;
        AudioSource music;
        int play = 0; 





        // Use this for initialization
        void Start()
        {
            focusable = GetComponent<FocusableBehaviour>();

            AddFocusEventHandlers();
            music = GetComponent<AudioSource>();
            
            
        }

        private void AddFocusEventHandlers()
        {
            focusable.ObjectFocused += focusable_ObjectFocused;
            focusable.ObjectFocusComplete += focusable_ObjectFocusComplete;
            focusable.ObjectFocusLost += focusable_ObjectFocusLost;
        }
        void OnEnable()
        {
            rend = GetComponent<Renderer>();
        }

        void focusable_ObjectFocusLost(object sender, FocusEventArgs eventArgs)
        {

        }

        void focusable_ObjectFocusComplete(object sender, FocusEventArgs eventArgs)
        {
            if (play == 0)
            {
                music.Play();
                play++;
            }
            

        }

        void focusable_ObjectFocused(object sender, FocusEventArgs eventArgs)
        {    
            
           
        }
        // Update is called once per frame
        void Update()
        {
            
        }



    }
}
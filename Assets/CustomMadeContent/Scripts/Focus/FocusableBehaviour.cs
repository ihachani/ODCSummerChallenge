﻿using UnityEngine;
using System.Collections;

namespace OrangeSummerChallenge.Focus
{
    public class FocusableBehaviour : MonoBehaviour, IFocusableBehavior
    {
        #region Private Fields
        [SerializeField]
        private float maxFocusTime;
        private float focusTime;
        /// <summary>
        /// Used to ensure that focus complet event is raised once.
        /// </summary>
        private bool focusComplete = false;
        #endregion
        // Use this for initialization
        #region UnityLifeCycleMethods
        void Start()
        {

        }
        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        public void Focused()
        {
            //focusTime += Time.fixedDeltaTime;
            if (focusTime + Time.fixedDeltaTime < maxFocusTime)
            {
                focusTime += Time.fixedDeltaTime;
                OnObjectFocused();
            }
            else
            {
                focusTime = maxFocusTime;
                OnObjectFocused();
                if (!focusComplete)
                {
                    OnObjectFocusComplete();
                    focusComplete = true;
                }
            }
        }
        public void FocusLost()
        {
            focusComplete = false;
            OnObjectFocusLost();
            focusTime = 0f;
            
        }



        #region Events
        public event FocusEvent ObjectFocused;
        private void OnObjectFocused()
        {
            if (ObjectFocused != null) // will be null if no subscribers
            {
                ObjectFocused(this, CreateFocusEventArgs());
            }
        }

        private FocusEventArgs CreateFocusEventArgs()
        {
            return new FocusEventArgs { GameObjectName = gameObject.name, FocusTime = focusTime ,GameObject = gameObject,Renderer = GetComponent<Renderer>(),MaxFocusTime = maxFocusTime };
        }
        public event FocusEvent ObjectFocusLost;
        private void OnObjectFocusLost()
        {
            if (ObjectFocusLost != null) // will be null if no subscribers
            {
                ObjectFocusLost(this, CreateFocusEventArgs());
            }
        }

        public event FocusEvent ObjectFocusComplete;
        private void OnObjectFocusComplete()
        {
            if (ObjectFocusComplete != null) // will be null if no subscribers
            {
                ObjectFocusComplete(this, CreateFocusEventArgs());
            }
        }
        #endregion
    }

}
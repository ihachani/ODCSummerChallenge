﻿using System;
using UnityEngine;

namespace OrangeSummerChallenge.Focus
{
    /// <summary>
    /// Event Args For the focus events.
    /// </summary>
    public class FocusEventArgs: EventArgs
    {
        string gameObjectName;

        public string GameObjectName
        {
            get { return gameObjectName; }
            set { gameObjectName = value; }
        }
        // Add needed Component As we need them.

        float focustTime;
        public float FocusTime
        {
            get { return focustTime; }
            set { focustTime = value; }
        }

        GameObject gameObject;

        public GameObject GameObject
        {
            get { return gameObject; }
            set { gameObject = value; }
        }
        Renderer renderer;

        public Renderer Renderer
        {
            get { return renderer; }
            set { renderer = value; }
        }
        float maxFocusTime;

        public float MaxFocusTime
        {
            get { return maxFocusTime; }
            set { maxFocusTime = value; }
        }

    }
}

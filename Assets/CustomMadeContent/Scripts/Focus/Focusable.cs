﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OrangeSummerChallenge.Focus
{
    /// <summary>
    /// NOT COMPLETED.
    /// </summary>
    class Focusable : IFocusableBehavior
    {
        private float focusTime;
        public void Focused()
        {
            throw new NotImplementedException();
        }

        public void FocusLost()
        {
            throw new NotImplementedException();
        }

        public event FocusEvent ObjectFocused;

        public event FocusEvent ObjectFocusLost;

        public event FocusEvent ObjectFocusComplete;
    }
}

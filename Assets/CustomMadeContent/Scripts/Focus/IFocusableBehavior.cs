﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OrangeSummerChallenge.Focus
{
    public delegate void FocusEvent(object sender, FocusEventArgs eventArgs);
    public interface IFocusableBehavior
    {
        void Focused();
        void FocusLost();

        event FocusEvent ObjectFocused;
        event FocusEvent ObjectFocusLost;
        event FocusEvent ObjectFocusComplete;
    }
}

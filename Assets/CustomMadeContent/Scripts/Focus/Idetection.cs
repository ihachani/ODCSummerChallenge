﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OrangeSummerChallenge.Focus
{
    public delegate void DetectionEvent(object sender, FocusEventArgs eventArgs);
    public interface IDetection
    {
        event DetectionEvent ObjectDetected;
        event DetectionEvent ObjectLost;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace OrangeSummerChallenge.Focus.Actions.TextureSwap
{
    [Serializable]
    public class TextureSwapAction : IFocusAction
    {
        [SerializeField]
        private Texture2D newTexture;
        [SerializeField]
        private string propertyNam;
        public void OnFocus(object sender, FocusEventArgs eventArgs)
        {
        }

        public void OnFocusComplete(object sender, FocusEventArgs eventArgs)
        {
            eventArgs.Renderer.material.SetTexture(propertyNam, newTexture);
        }

        public void OnFocusLost(object sender, FocusEventArgs eventArgs)
        {
        }
    }
}

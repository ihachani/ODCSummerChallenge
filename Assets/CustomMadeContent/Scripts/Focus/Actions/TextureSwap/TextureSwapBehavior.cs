﻿using UnityEngine;
using System.Collections;
using OrangeSummerChallenge.Focus.VR;

namespace OrangeSummerChallenge.Focus.Actions.TextureSwap
{
    [RequireComponent(typeof(FocusableBehaviour))]
    [RequireComponent(typeof(GazableBehavior))]
    public class TextureSwapBehavior : MonoBehaviour
    {

        [SerializeField]
        private TextureSwapAction textureSwipeAction;
        IFocusableBehavior focusable;
        void Start()
        {
            focusable = GetComponent<FocusableBehaviour>();

            focusable.ObjectFocusComplete += textureSwipeAction.OnFocusComplete;
        }
    }
    
}
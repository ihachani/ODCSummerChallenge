﻿using OrangeSummerChallenge.Focus.VR;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace OrangeSummerChallenge.Focus.Actions.FocusToRotate
{
    [RequireComponent(typeof(FocusableBehaviour))]
    [RequireComponent(typeof(GazableBehavior))]
    class RotateBehaviour : MonoBehaviour
    {
        [SerializeField]
        private RotateAction rt;

        IFocusableBehavior focusable;
        void Start()
        {
            focusable = GetComponent<FocusableBehaviour>();
            focusable.ObjectFocusComplete += rt.OnFocusComplete;
        }
    }
}

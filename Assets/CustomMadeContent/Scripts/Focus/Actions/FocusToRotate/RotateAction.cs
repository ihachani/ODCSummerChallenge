﻿using System;
using System.Collections.Generic;
using UnityEngine;


namespace OrangeSummerChallenge.Focus.Actions.FocusToRotate
{
    [Serializable]
    class RotateAction : IFocusAction
    {
        [SerializeField]
        private Transform rotator;

        public void OnFocus(object sender, FocusEventArgs eventArgs)
        {
            throw new NotImplementedException();
        }

        public void OnFocusComplete(object sender, FocusEventArgs eventArgs)
        {
            rotator.Rotate( new Vector3(0,1,0)*180);
        }

        public void OnFocusLost(object sender, FocusEventArgs eventArgs)
        {
            throw new NotImplementedException();
        }
    }
}

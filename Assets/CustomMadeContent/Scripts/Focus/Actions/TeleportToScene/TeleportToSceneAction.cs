﻿using OrangeSummerChallenge.Manager;
using System;
using UnityEngine;

namespace OrangeSummerChallenge.Focus.Actions.TeleportToScene
{
    [Serializable]
    public class TeleportToSceneAction : IFocusAction
    {
        [SerializeField]
        private SceneManager mng;
        public void OnFocus(object sender, FocusEventArgs eventArgs)
        {
        }

        public void OnFocusComplete(object sender, FocusEventArgs eventArgs)
        {
            mng.LoadNextScene();
        }

        public void OnFocusLost(object sender, FocusEventArgs eventArgs)
        {
        }
    }
}

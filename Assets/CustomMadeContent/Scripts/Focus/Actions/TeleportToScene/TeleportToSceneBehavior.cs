﻿using UnityEngine;
using System.Collections;
using OrangeSummerChallenge.Focus.VR;

namespace OrangeSummerChallenge.Focus.Actions.TeleportToScene
{
    [RequireComponent(typeof(FocusableBehaviour))]
    [RequireComponent(typeof(GazableBehavior))]
    public class TeleportToSceneBehavior : MonoBehaviour
    {

        [SerializeField]
        private TeleportToSceneAction teleportToSceneAction;
        IFocusableBehavior focusable;

        void Start()
        {
            focusable = GetComponent<FocusableBehaviour>();

            focusable.ObjectFocusComplete += teleportToSceneAction.OnFocusComplete;
        }
    } 
}

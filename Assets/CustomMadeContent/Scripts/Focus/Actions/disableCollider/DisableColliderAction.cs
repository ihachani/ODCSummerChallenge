﻿using System;
using System.Collections.Generic;

using System.Text;
using UnityEngine;

namespace OrangeSummerChallenge.Focus.Actions.disableCollider
{
    [Serializable]
    class DisableColliderAction : IFocusAction
    {
        [SerializeField]
        private GameObject obj;

        public void OnFocus(object sender, FocusEventArgs eventArgs)
        {
            throw new NotImplementedException();
        }

        public void OnFocusComplete(object sender, FocusEventArgs eventArgs)
        {
            Debug.Log("Collider Focused");
            Collider[] col =  obj.GetComponents<Collider>() ;


            for (int i = 0; i < col.Length; i++ )
            {
                if (!col[i].isTrigger)
                {
                    col[i].enabled = false;
                }
            }
        }

        public void OnFocusLost(object sender, FocusEventArgs eventArgs)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using OrangeSummerChallenge.Focus.VR;

namespace OrangeSummerChallenge.Focus.Actions.disableCollider
{
    [RequireComponent(typeof(FocusableBehaviour))]
    [RequireComponent(typeof(GazableBehavior))]
    public class DisableColliderBehaviour : MonoBehaviour
    {
        [SerializeField]
        private DisableColliderAction disableAction;
        IFocusableBehavior focusable;
        // Use this for initialization
        void Start()
        {
            focusable = GetComponent<FocusableBehaviour>();

            focusable.ObjectFocusComplete += disableAction.OnFocusComplete;

        }
    }
}
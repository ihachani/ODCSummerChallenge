﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
namespace OrangeSummerChallenge.Focus.Actions.ColorSwap
{
    [Serializable]
    public class ColorSwapAction : IFocusAction
    {
        [SerializeField]
        private Color newColor;
        [SerializeField]
        private string propertyName;
        public void OnFocus(object sender, FocusEventArgs eventArgs)
        {
        }
        public void OnFocusComplete(object sender, FocusEventArgs eventArgs)
        {
            
            eventArgs.Renderer.material.SetColor(propertyName, newColor);
        }
        public void OnFocusLost(object sender, FocusEventArgs eventArgs)
        {

        }
    }
}

﻿using UnityEngine;
using System.Collections;
using OrangeSummerChallenge.Focus.VR;

namespace OrangeSummerChallenge.Focus.Actions.ColorSwap
{
    [RequireComponent(typeof(FocusableBehaviour))]
    [RequireComponent(typeof(GazableBehavior))]
    public class ColorSwapBehavior : MonoBehaviour
    {


        [SerializeField]
        private ColorSwapAction colorSwipeAction;
        IFocusableBehavior focusable;
        void Start()
        {
            focusable = GetComponent<FocusableBehaviour>();

            focusable.ObjectFocusComplete += colorSwipeAction.OnFocusComplete;
        }

    }
    
}
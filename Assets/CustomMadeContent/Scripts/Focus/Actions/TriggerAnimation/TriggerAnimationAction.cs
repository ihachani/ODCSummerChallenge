﻿using System;
using System.Collections.Generic;
using UnityEngine;


namespace OrangeSummerChallenge.Focus.Actions.TriggerAnimation
{
    [Serializable]
    class TriggerAnimationAction : IFocusAction
    {
        [SerializeField]
        private Animator targetAnimator;
        [SerializeField]
        private string triggerName;

        public void OnFocus(object sender, FocusEventArgs eventArgs)
        {
        }

        public void OnFocusComplete(object sender, FocusEventArgs eventArgs)
        {
            targetAnimator.SetTrigger(triggerName);
        }

        public void OnFocusLost(object sender, FocusEventArgs eventArgs)
        {
        }
    }
}

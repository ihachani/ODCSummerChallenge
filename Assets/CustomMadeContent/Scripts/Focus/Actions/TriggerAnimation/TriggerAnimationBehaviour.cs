﻿using UnityEngine;
using System.Collections;
using OrangeSummerChallenge.Focus.VR;

namespace OrangeSummerChallenge.Focus.Actions.TriggerAnimation
{
    [RequireComponent(typeof(FocusableBehaviour))]
    [RequireComponent(typeof(GazableBehavior))]
    public class TriggerAnimationBehaviour : MonoBehaviour
    {

        [SerializeField]
        private TriggerAnimationAction triggerAnimationAction;
        IFocusableBehavior focusable;
        void Start()
        {
            focusable = GetComponent<FocusableBehaviour>();

            focusable.ObjectFocusComplete += triggerAnimationAction.OnFocusComplete;
        }
    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace OrangeSummerChallenge.Focus.Actions.FocusToStopSound
{
    [Serializable]
    class StopSoundAction : IFocusAction
    {
        [SerializeField]
        private AudioSource toStop;
        public void OnFocus(object sender, FocusEventArgs eventArgs)
        {
            throw new NotImplementedException();
        }

        public void OnFocusComplete(object sender, FocusEventArgs eventArgs)
        {
            toStop.Stop();
            
        }

        public void OnFocusLost(object sender, FocusEventArgs eventArgs)
        {
            throw new NotImplementedException();
        }
    }
}

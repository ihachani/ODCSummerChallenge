﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
namespace OrangeSummerChallenge.Focus.Actions.SoundActivation
{

    [Serializable]
    public class SoundAction : IFocusAction
    {
        [SerializeField]
        private AudioSource sound;

        private bool played=false; 


        public void OnFocus(object sender, FocusEventArgs eventArgs)
        {
            throw new NotImplementedException();
        }

        public void OnFocusComplete(object sender, FocusEventArgs eventArgs)
        {
            if (!played) {
                sound.Play();
                played = true; 
            }
            
        }

        public void OnFocusLost(object sender, FocusEventArgs eventArgs)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using UnityEngine;
using OrangeSummerChallenge.Focus.VR;

namespace OrangeSummerChallenge.Focus.Actions.SoundActivation
{

    [RequireComponent(typeof(FocusableBehaviour))]
    [RequireComponent(typeof(GazableBehavior))]
    public class SoundBehaviour : MonoBehaviour
    {
        [SerializeField]
        private SoundAction soundAction;
        IFocusableBehavior focusable;

        void Start()
        {
            focusable = GetComponent<FocusableBehaviour>();

            focusable.ObjectFocusComplete += soundAction.OnFocusComplete;
        }
    }
}

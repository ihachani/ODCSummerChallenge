﻿using UnityEngine;
using System.Collections;
using OrangeSummerChallenge.Focus.VR;

namespace OrangeSummerChallenge.Focus.Actions.Collecting
{
    [RequireComponent(typeof(FocusableBehaviour))]
    [RequireComponent(typeof(GazableBehavior))]
    public class CollectBehavior : MonoBehaviour
    {

        [SerializeField]
        private CollectAction collectableAction;
        IFocusableBehavior focusable;
        void Start()
        {
            focusable = GetComponent<FocusableBehaviour>();

            focusable.ObjectFocusComplete += collectableAction.OnFocusComplete;
        }
    }
    
}
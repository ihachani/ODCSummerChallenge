﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace OrangeSummerChallenge.Focus.Actions.Collecting
{
    [Serializable]
    public class CollectAction : IFocusAction
    {
        [SerializeField]
        private ItemsCollected itemsCollected;
        [SerializeField]
        private Collectable collectable;
        public void OnFocus(object sender, FocusEventArgs eventArgs)
        {
        }

        public void OnFocusComplete(object sender, FocusEventArgs eventArgs)
        {
            eventArgs.GameObject.SetActive(false);
            itemsCollected.addItem(collectable);
        }

        public void OnFocusLost(object sender, FocusEventArgs eventArgs)
        {
        }
    }
}

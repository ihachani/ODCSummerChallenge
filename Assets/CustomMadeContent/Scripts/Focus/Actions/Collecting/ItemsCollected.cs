﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


namespace OrangeSummerChallenge.Focus.Actions.Collecting
{
    /// <summary>
    /// This Class is Designed for 
    /// Score and collected Items 
    /// 
    /// At the end of the Game All Values Collected must return to 0.
    /// </summary>

    public class ItemsCollected :MonoBehaviour
    {

        //[SerializeField]
        //public static int LimitKey;
        //[SerializeField]
        //public static int ClefCollected = 0;
        [SerializeField]
        private  List<Collectable> collectables = new List<Collectable>();



        public  bool seekElement(string id)
        {
            foreach(var element in collectables){
                if (element.Id == id)
                    return true;
            }
            return false;
        }
        /// <summary>
        /// method used to add a collectable.
        /// </summary>
        /// <param name="collectable"></param>

        public  void addItem(Collectable collectable)
        {

            collectables.Add(collectable);
            Debug.Log("object collected"+collectables.Count);

        }


        //public static bool HasEnoughtKey(int k)
        //{

        //    if (ClefCollected >= k)
        //    {

        //        return true;
        //    }
        //    else return false;


        //}
        /// <summary>
        /// method used to remove a collectable
        /// </summary>
        /// <param name="collectable"></param>
        public  void removeItem(Collectable collectable)
        {

            collectables.Remove(collectable);

        }






    }



    
}
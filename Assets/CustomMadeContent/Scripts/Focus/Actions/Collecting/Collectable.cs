﻿using UnityEngine;
using System.Collections;
using System;

namespace OrangeSummerChallenge.Focus.Actions.Collecting
{
    [Serializable]
    public class Collectable
    {
        [SerializeField]
        private string id;

        public string Id
        {
            get { return Id; }
            set { Id = value; }
        }

        [SerializeField]
        private string Nom;

        public string Nom1
        {
            get { return Nom; }
            set { Nom = value; }
        }
    }

}
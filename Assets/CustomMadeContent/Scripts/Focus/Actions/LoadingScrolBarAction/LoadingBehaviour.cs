﻿using UnityEngine;
using System.Collections;
using OrangeSummerChallenge.Focus;
using OrangeSummerChallenge.Focus.VR;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(FocusableBehaviour))]
[RequireComponent(typeof(GazableBehavior))]
public class LoadingBehaviour : MonoBehaviour {
    [SerializeField]
    private Image circleImage;
    [SerializeField]
    private LoadingAction loadingAction;
    IFocusableBehavior focusable;

    void Awake()
    {
        circleImage.type = Image.Type.Filled;
        circleImage.fillMethod = Image.FillMethod.Radial360;
        circleImage.fillOrigin = 0;
        circleImage.fillAmount = 0;
    }
	// Use this for initialization	
    void Start () {
        focusable = GetComponent<FocusableBehaviour>();
        //loadingAction.CircleImage = CircleImage;
        focusable.ObjectFocused +=loadingAction.OnFocus ;
        focusable.ObjectFocusLost += loadingAction.OnFocusLost;
        focusable.ObjectFocusComplete += loadingAction.OnFocusComplete;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

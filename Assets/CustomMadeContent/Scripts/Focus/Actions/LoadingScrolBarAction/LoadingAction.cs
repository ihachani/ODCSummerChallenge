﻿using UnityEngine;
using System;
using System.Collections;
using OrangeSummerChallenge.Focus.Actions;
using OrangeSummerChallenge.Focus;
using UnityEngine.UI;
using UnityEngine.EventSystems;
[Serializable]
public class LoadingAction : IFocusAction{
    [SerializeField]
    private Image circleImage;
    [SerializeField]
    private AudioSource winnerAudioClip;

    public void OnFocus(object sender, FocusEventArgs eventArgs)
    {
        if (eventArgs.MaxFocusTime != 0)
        {
            circleImage.fillAmount = Mathf.Max(eventArgs.FocusTime / eventArgs.MaxFocusTime, 0.001f);
        }
        else 
        {
            Debug.Log("max Focus Time can't be null");
        }
        
    }

    public void OnFocusComplete(object sender, FocusEventArgs eventArgs)
    {
        if (winnerAudioClip != null)
        {
            Debug.Log("winning clip");
            winnerAudioClip.Play();
        }
    }

    public void OnFocusLost(object sender, FocusEventArgs eventArgs)
    {
        circleImage.fillAmount=0;
    }
}

﻿using UnityEngine;
using System.Collections;
using OrangeSummerChallenge.Focus.VR;


namespace OrangeSummerChallenge.Focus.Actions.DisableFreind
{

    [RequireComponent(typeof(FocusableBehaviour))]
    [RequireComponent(typeof(GazableBehavior))]
    public class DisbaleFreindBehaviour : MonoBehaviour
    {
        [SerializeField]
        private DisableFriendAction act;
        IFocusableBehavior focusable;


        void Start()
        {
            focusable = GetComponent<FocusableBehaviour>();

            focusable.ObjectFocusComplete += act.OnFocusComplete;
        }
    }
}
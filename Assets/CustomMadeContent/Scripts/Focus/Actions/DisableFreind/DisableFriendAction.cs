﻿using UnityEngine;
using System.Collections;
using OrangeSummerChallenge.Focus.Actions;
using System;
using System.Collections.Generic ;
using OrangeSummerChallenge.Focus;
using OrangeSummerChallenge.Focus.Actions.ColorSwap; 

[Serializable]
public class DisableFriendAction : IFocusAction{

    [SerializeField]
    private List<GameObject> neighbours;

    private int inte = 0;

    public void OnFocus(object sender, OrangeSummerChallenge.Focus.FocusEventArgs eventArgs)
    {
        
    }

    public void OnFocusComplete(object sender, OrangeSummerChallenge.Focus.FocusEventArgs eventArgs)
    {
        if (inte == 0)
        {
            for (int i = 0; i < neighbours.Count; i++)
            {

                neighbours[i].SetActive(false); 
              

            }
            inte++;
        }
        
    }

    public void OnFocusLost(object sender, OrangeSummerChallenge.Focus.FocusEventArgs eventArgs)
    {
        
    }
}

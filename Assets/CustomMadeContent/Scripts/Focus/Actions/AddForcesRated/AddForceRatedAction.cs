﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OrangeSummerChallenge.Focus.Actions.AddForcesRated
{
   [Serializable]
    class AddForceRatedAction : IFocusAction
    {
       [SerializeField]
       private MonoBehaviour toStartCouroutine;

        [SerializeField]
        private Rigidbody rgd;

        [SerializeField]
        private Vector3 forceVector;

        [SerializeField]
        private Vector3 antiForce;

        [SerializeField]
        private Vector3 annulatorForce;

        [SerializeField]
        private int rate;


        private bool test = false;

        [SerializeField]
        private float timeToWait;
        public void OnFocus(object sender, FocusEventArgs eventArgs)
        {
            throw new NotImplementedException();
        }

        public void OnFocusComplete(object sender, FocusEventArgs eventArgs)
        {
            if (!test)
            {
                rgd.AddForce(forceVector * rate);
                Debug.Log("Force Added : X =  " + forceVector.x + " Y = " + forceVector.y);
                toStartCouroutine.StartCoroutine(WaitToForce());
                test = true;

            }
        }

        public void OnFocusLost(object sender, FocusEventArgs eventArgs)
        {
            throw new NotImplementedException();
        }

        IEnumerator WaitToForce() {

            yield return new WaitForSeconds(timeToWait);
            rgd.AddForce(antiForce * rate);
            Debug.Log("Anti Force Applicated");
            yield return new WaitForSeconds(timeToWait);
            rgd.AddForce(annulatorForce * rate);
            Debug.Log("Annulator Force Applicated");
             yield return null;

        
        }
    }
}

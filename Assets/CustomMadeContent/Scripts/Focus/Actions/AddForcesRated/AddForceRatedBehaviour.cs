﻿using OrangeSummerChallenge.Focus.VR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


namespace OrangeSummerChallenge.Focus.Actions.AddForcesRated
{
    [RequireComponent(typeof(FocusableBehaviour))]
    [RequireComponent(typeof(GazableBehavior))]
    class AddForceRatedBehaviour : MonoBehaviour
    {
        [SerializeField]
        private AddForceRatedAction forceAction;
        IFocusableBehavior focusable;

        void Start()
        {   

            focusable = GetComponent<FocusableBehaviour>();

            focusable.ObjectFocusComplete += forceAction.OnFocusComplete;
            
        }

    }
}

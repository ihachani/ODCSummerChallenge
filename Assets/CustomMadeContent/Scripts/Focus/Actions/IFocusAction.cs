﻿using UnityEngine;
using System.Collections;
using OrangeSummerChallenge.Focus.VR;

namespace OrangeSummerChallenge.Focus.Actions
{

    public interface IFocusAction
    {
        void OnFocus(object sender, FocusEventArgs eventArgs);
        void OnFocusComplete(object sender, FocusEventArgs eventArgs);
        void OnFocusLost(object sender, FocusEventArgs eventArgs);
    }

}
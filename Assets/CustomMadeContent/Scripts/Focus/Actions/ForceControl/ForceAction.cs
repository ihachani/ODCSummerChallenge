﻿using OrangeSummerChallenge.Focus.Actions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace OrangeSummerChallenge.Focus.Actions.ForceControl
{
    [Serializable]
    class ForceAction : IFocusAction
    {
        
        [SerializeField]
        private Animator go;
        [SerializeField]
        private String triggerName;

     


       
        public void OnFocus(object sender, OrangeSummerChallenge.Focus.FocusEventArgs eventArgs)
        {
            throw new NotImplementedException();
        }

        public void OnFocusComplete(object sender, OrangeSummerChallenge.Focus.FocusEventArgs eventArgs)
        {
            
                go.SetTrigger(triggerName);
           

        }

        public void OnFocusLost(object sender, OrangeSummerChallenge.Focus.FocusEventArgs eventArgs)
        {
            throw new NotImplementedException();
        }

        //IEnumerator TranslationCouroutine()
        //{
        //    Vector3 pos = new Vector3(go.transform.position.x + deviation.x, go.transform.position.y + deviation.y, go.transform.position.z + deviation.z);
        //    Debug.Log("position checked: " + pos);
        //    for (float i = 0; i < 1; i += Time.deltaTime * 5)
        //    {
        //        go.transform.position = Vector3.Lerp(go.transform.position, pos, i);
        //        yield return new WaitForSeconds(0.2f);
        //    }
        //}
    }
}

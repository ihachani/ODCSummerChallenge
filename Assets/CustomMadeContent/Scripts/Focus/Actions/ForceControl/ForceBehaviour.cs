﻿using OrangeSummerChallenge.Focus.VR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace OrangeSummerChallenge.Focus.Actions.ForceControl
{
    [RequireComponent(typeof(FocusableBehaviour))]
    [RequireComponent(typeof(GazableBehavior))]
    class ForceBehaviour:MonoBehaviour
    {
        [SerializeField]
       private ForceAction force;
        IFocusableBehavior focusable;
        void Start()
        {
            focusable = GetComponent<FocusableBehaviour>();
            focusable.ObjectFocusComplete += force.OnFocusComplete;
        }
    }
}

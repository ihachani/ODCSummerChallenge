﻿using OrangeSummerChallenge.Focus;
using OrangeSummerChallenge.Focus.VR;
using System;
using System.Collections.Generic;

using UnityEngine;

namespace Assets.CustomMadeContent.Scripts.Focus.Actions.ColorSwapCollider
{
    [RequireComponent(typeof(FocusableBehaviour))]
    [RequireComponent(typeof(GazableBehavior))]
    class ColorSwapColliderBehaviour :  MonoBehaviour
    {
      

        [SerializeField]
        private ColorSwapColliderAction colorSwipeAction;
       IFocusableBehavior focusable;
       void Start()
       {
           focusable = GetComponent<FocusableBehaviour>();

           focusable.ObjectFocusComplete += colorSwipeAction.OnFocusComplete;
       }


    }
}

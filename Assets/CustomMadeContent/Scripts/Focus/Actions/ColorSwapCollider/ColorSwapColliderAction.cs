﻿using OrangeSummerChallenge.Focus;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.CustomMadeContent.Scripts.Focus.Actions.ColorSwapCollider
{

    [Serializable]
    class ColorSwapColliderAction
    {
        [SerializeField]
        private Color newColor;
        [SerializeField]
        private string propertyName;
        [SerializeField]
        private Renderer objectToChange;
        public void OnFocus(object sender, FocusEventArgs eventArgs)
        {
        }
        public void OnFocusComplete(object sender, FocusEventArgs eventArgs)
        {

            objectToChange.material.SetColor(propertyName, newColor);
        }
        public void OnFocusLost(object sender, FocusEventArgs eventArgs)
        {

        }
    }
}

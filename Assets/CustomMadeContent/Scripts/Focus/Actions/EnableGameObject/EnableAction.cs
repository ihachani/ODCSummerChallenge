﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace OrangeSummerChallenge.Focus.Actions.EnableGameObject
{
    [Serializable]
    public class EnableAction : IFocusAction
    {
        [SerializeField]
        private List<GameObject> gameObjects;
        private bool activatedGameObjects = false;
        public void OnFocus(object sender, FocusEventArgs eventArgs)
        {
        }

        public void OnFocusComplete(object sender, FocusEventArgs eventArgs)
        {
            if (!activatedGameObjects)
            {
                foreach (var gameObject in gameObjects)
                {
                    gameObject.SetActive(true);
                }
                activatedGameObjects = true;
            }
        }

        public void OnFocusLost(object sender, FocusEventArgs eventArgs)
        {
        }
    }
}

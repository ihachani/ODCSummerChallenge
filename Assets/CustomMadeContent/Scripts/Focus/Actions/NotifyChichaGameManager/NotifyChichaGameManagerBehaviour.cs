﻿using UnityEngine;
using System.Collections;
using OrangeSummerChallenge.Focus.VR;
using OrangeSummerChallenge.Managers;

namespace OrangeSummerChallenge.Focus.Actions.NotifyChichaGameManager
{
    [RequireComponent(typeof(FocusableBehaviour))]
    [RequireComponent(typeof(GazableBehavior))]
    public class NotifyChichaGameManagerBehaviour : MonoBehaviour
    {

        [SerializeField]
        private NotifyChichaGameManagerAction notifyChichaGameManagerAction;
        IFocusableBehavior focusable;

        void Start()
        {
            focusable = GetComponent<FocusableBehaviour>();

            focusable.ObjectFocusComplete += notifyChichaGameManagerAction.OnFocusComplete;
        }

        #region Events
        public event ChichaFocused chichaFocused
        {
            add
            {
                notifyChichaGameManagerAction.chichaFocused += value;
            }
            remove
            {
                notifyChichaGameManagerAction.chichaFocused -= value;
            }
        }
        #endregion
    }

}
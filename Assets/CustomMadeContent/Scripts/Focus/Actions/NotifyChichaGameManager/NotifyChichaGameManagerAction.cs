﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OrangeSummerChallenge.Focus.Actions.NotifyChichaGameManager
{
    public delegate void ChichaFocused(object sender, EventArgs eventArgs);
    [Serializable]
    class NotifyChichaGameManagerAction : IFocusAction
    {
        public void OnFocus(object sender, FocusEventArgs eventArgs)
        {
        }

        public void OnFocusComplete(object sender, FocusEventArgs eventArgs)
        {
            OnChichaFocused();
        }

        public void OnFocusLost(object sender, FocusEventArgs eventArgs)
        {
        }

        #region Events
        public event ChichaFocused chichaFocused;
        private void OnChichaFocused()
        {
            if (chichaFocused != null)
            {
                chichaFocused(this,new EventArgs());
            }
        }
        #endregion
    }
}

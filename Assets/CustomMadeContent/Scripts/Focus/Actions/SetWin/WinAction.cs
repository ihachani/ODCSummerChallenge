﻿using UnityEngine;
using System.Collections;
using System;
using OrangeSummerChallenge.Stairs;



namespace OrangeSummerChallenge.Focus.Actions.SetWin 
{
  //  public delegate void SuccessRowEventHandler(object source, EventArgs e);
    [Serializable]
    public class WinAction : IFocusAction
    {
        [SerializeField]
        private StairsMaster Manager;

        private bool exec = false;


        public void OnFocus(object sender, FocusEventArgs eventArgs)
        {
         
        }

        public void OnFocusComplete(object sender, FocusEventArgs eventArgs)
        {
            if (!exec)
            {
                Manager.PlayGame();
                exec = true;
            }
        }

        public void OnFocusLost(object sender, FocusEventArgs eventArgs)
        {
            throw new System.NotImplementedException();
        }
    }

}
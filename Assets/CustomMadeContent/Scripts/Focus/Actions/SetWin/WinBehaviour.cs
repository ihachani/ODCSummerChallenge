﻿using UnityEngine;
using System.Collections;
using OrangeSummerChallenge.Focus.VR;

namespace OrangeSummerChallenge.Focus.Actions.SetWin
{
    [RequireComponent(typeof(FocusableBehaviour))]
    [RequireComponent(typeof(GazableBehavior))]
    public class WinBehaviour : MonoBehaviour
    {
        [SerializeField]
        WinAction winAction;
        IFocusableBehavior focusable;

        void Start()
        {
            focusable = GetComponent<FocusableBehaviour>();
            focusable.ObjectFocusComplete += winAction.OnFocusComplete;
        }

        /*
        public event SuccessRowEventHandler successRow
        {
            add
            {
                winAction.successRow += value;
            }
            remove
            {
                winAction.successRow -= value;
            }
        }
         */
        
    }

}
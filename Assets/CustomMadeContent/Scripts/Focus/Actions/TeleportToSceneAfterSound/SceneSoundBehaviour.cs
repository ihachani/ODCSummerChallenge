﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace OrangeSummerChallenge.Focus.Actions.TeleportToSceneAfterSound
{
    class SceneSoundBehaviour : MonoBehaviour
    {
        [SerializeField]
        private SceneSoundAction snd;
        IFocusableBehavior focusable;

        void Start()
        {
            focusable = GetComponent<FocusableBehaviour>();

            focusable.ObjectFocusComplete += snd.OnFocusComplete;
        }

    }
}

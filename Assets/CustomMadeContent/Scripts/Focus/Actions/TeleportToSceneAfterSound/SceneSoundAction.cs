﻿using OrangeSummerChallenge.Manager;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace OrangeSummerChallenge.Focus.Actions.TeleportToSceneAfterSound
{
    [Serializable]
    class SceneSoundAction : IFocusAction
    {
        [SerializeField]
        private SceneManager mng;

        [SerializeField]
        private AudioSource story;

        [SerializeField]
        private MonoBehaviour toStartCouroutine;

        public void OnFocus(object sender, FocusEventArgs eventArgs)
        {
            throw new NotImplementedException();
        }

        public void OnFocusComplete(object sender, FocusEventArgs eventArgs)
        {
            story.Play();
            toStartCouroutine.StartCoroutine(LoadScene());
            
            
        }

        IEnumerator LoadScene() {

            yield return new WaitForSeconds(story.clip.length);
            mng.LoadNextScene();
        
        }

        public void OnFocusLost(object sender, FocusEventArgs eventArgs)
        {
            throw new NotImplementedException();
        }
    }
}

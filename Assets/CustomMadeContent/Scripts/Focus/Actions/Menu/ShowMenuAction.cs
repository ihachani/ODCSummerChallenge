﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace OrangeSummerChallenge.Focus.Actions.Menu
{
    /// <summary>
    /// Used to Show Menu for Duration.
    /// Might Look into disabling gameObject After hiding it if performence drops.
    /// </summary>
    [Serializable]
    public class ShowMenuAction : IFocusAction
    {
        [SerializeField]
        private CanvasGroup canvasGroup;
        [SerializeField]
        private GameObject Menu;
        [SerializeField]
        private float fadeSpeed;
        [SerializeField]
        private float menuDuration;
        private MonoBehaviour monoBehavior;

        public MonoBehaviour MonoBehavior
        {
            get { return monoBehavior; }
            set { monoBehavior = value; }
        }

        public void OnFocus(object sender, FocusEventArgs eventArgs)
        {
        }

        public void OnFocusComplete(object sender, FocusEventArgs eventArgs)
        {
            ShowMenu();
            
        }

        private void ShowMenu()
        {
            monoBehavior.StopAllCoroutines();
            Menu.SetActive(true);
            monoBehavior.StartCoroutine(IncreaseAlphaTo1());
            canvasGroup.blocksRaycasts = true;
            canvasGroup.interactable = true;
        }
        IEnumerator IncreaseAlphaTo1()
        {
            while (canvasGroup.alpha < 1)
            {
                yield return null;
                canvasGroup.alpha += Time.deltaTime ;
            }
        }
        IEnumerator hideMenuAfterDuration()
        {
            while (canvasGroup.alpha < 1)
            {
                yield return null;
            }
            yield return new WaitForSeconds(menuDuration);
            HideMenu();
        }

        private void HideMenu()
        {
            monoBehavior.StartCoroutine(DecreaseAlphaTo0());
            canvasGroup.blocksRaycasts = false;
            canvasGroup.interactable = false;
            monoBehavior.StartCoroutine(WaitForMenToHideAndDisableIt());
        }
        IEnumerator DecreaseAlphaTo0()
        {
            while (canvasGroup.alpha > 0)
            {
                yield return null;
                canvasGroup.alpha -= Time.deltaTime ;
            }
        }
        IEnumerator WaitForMenToHideAndDisableIt()
        {
            while (canvasGroup.alpha > 0)
            {
                yield return null;
            }
            Menu.SetActive(false);
        }
        public void OnFocusLost(object sender, FocusEventArgs eventArgs)
        {
            monoBehavior.StopAllCoroutines();
            monoBehavior.StartCoroutine(hideMenuAfterDuration());
        }
    }
}

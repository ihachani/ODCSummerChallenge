﻿using UnityEngine;
using System.Collections;
using OrangeSummerChallenge.Focus.VR;


namespace OrangeSummerChallenge.Focus.Actions.Menu
{
    [RequireComponent(typeof(FocusableBehaviour))]
    [RequireComponent(typeof(GazableBehavior))]
    public class ShowMenuBehavior : MonoBehaviour
    {

        [SerializeField]
        private ShowMenuAction showMenuAction;
        IFocusableBehavior focusable;

        void Start()
        {
            focusable = GetComponent<FocusableBehaviour>();
            showMenuAction.MonoBehavior = this;
            focusable.ObjectFocusComplete += showMenuAction.OnFocusComplete;
            focusable.ObjectFocusLost += showMenuAction.OnFocusLost;
        }
    }

}
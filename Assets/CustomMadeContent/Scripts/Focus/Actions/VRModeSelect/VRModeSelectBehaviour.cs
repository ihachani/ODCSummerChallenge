﻿using UnityEngine;
using System.Collections;
using OrangeSummerChallenge.Focus.VR;

namespace OrangeSummerChallenge.Focus.Actions.VRModeSelect
{
    [RequireComponent(typeof(FocusableBehaviour))]
    [RequireComponent(typeof(GazableBehavior))]
    public class VRModeSelectBehaviour : MonoBehaviour
    {

       [SerializeField]
        private VRModeSelectAction vrModeSelectAction;
        IFocusableBehavior focusable;
        void Start()
        {
            focusable = GetComponent<FocusableBehaviour>();

            focusable.ObjectFocusComplete += vrModeSelectAction.OnFocusComplete;
        }
    }
    
}
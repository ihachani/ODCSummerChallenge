﻿using System;
using System.Collections.Generic;
using UnityEngine;
using OrangeSummerChallenge.JsonSerialisation.SaveVR;

namespace OrangeSummerChallenge.Focus.Actions.VRModeSelect
{
    [Serializable]
    public class VRModeSelectAction : IFocusAction
    {
        [SerializeField]
        private ChoosingControll choosingControll;
        /// <summary>
        /// Used to select VR mode or Mobile mode. True for VR.
        /// </summary>
        [SerializeField]
        private bool VRModeCheckBox;
        public void OnFocus(object sender, FocusEventArgs eventArgs)
        {

        }

        public void OnFocusComplete(object sender, FocusEventArgs eventArgs)
        {
            if (VRModeCheckBox)
                choosingControll.CardboardChoice();
            else
                choosingControll.MobileChoice();
        }

        public void OnFocusLost(object sender, FocusEventArgs eventArgs)
        {

        }
    }
}

﻿using OrangeSummerChallenge.Actions;
using OrangeSummerChallenge.Focus.Actions;
using System;
using UnityEngine;


namespace Assets.CustomMadeContent.Scripts.Focus.Actions.FocusToShare
{
    [Serializable]
    class ShareAction : IFocusAction

    {
        [SerializeField]
        private FBHolder facebookHolder;


        private bool shared = false;



        public void OnFocus(object sender, OrangeSummerChallenge.Focus.FocusEventArgs eventArgs)
        {
            throw new NotImplementedException();
        }

        public void OnFocusComplete(object sender, OrangeSummerChallenge.Focus.FocusEventArgs eventArgs)
        {
            if (!shared)
            {
                facebookHolder.ShareWithFreinds();
                shared = true;
            }
        }

        public void OnFocusLost(object sender, OrangeSummerChallenge.Focus.FocusEventArgs eventArgs)
        {
            throw new NotImplementedException();
        }
    }
}

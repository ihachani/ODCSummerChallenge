﻿using OrangeSummerChallenge.Focus;
using OrangeSummerChallenge.Focus.VR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.CustomMadeContent.Scripts.Focus.Actions.FocusToShare
{

    [RequireComponent(typeof(FocusableBehaviour))]
    [RequireComponent(typeof(GazableBehavior))]
    class ShareBehaviour : MonoBehaviour
    {
        [SerializeField]
        private ShareAction shareAction;

        IFocusableBehavior focusable;



        void Start()
        {
            focusable = GetComponent<FocusableBehaviour>();

            focusable.ObjectFocusComplete += shareAction.OnFocusComplete;
        }

    }
}

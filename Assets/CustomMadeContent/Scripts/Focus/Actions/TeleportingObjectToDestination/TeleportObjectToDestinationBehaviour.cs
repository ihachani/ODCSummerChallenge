﻿using UnityEngine;
using System.Collections;
using OrangeSummerChallenge.Focus.VR;

namespace OrangeSummerChallenge.Focus.Actions.TeleportingObjectToDestination
{
    [RequireComponent(typeof(FocusableBehaviour))]
    [RequireComponent(typeof(GazableBehavior))]
    public class TeleportObjectToDestinationBehaviour : MonoBehaviour
    {

        [SerializeField]
        private TeleportObjectToDestinationAction teleportObjectToDestinationAction;
        IFocusableBehavior focusable;

        void Start()
        {
            focusable = GetComponent<FocusableBehaviour>();

            focusable.ObjectFocusComplete += teleportObjectToDestinationAction.OnFocusComplete;
        }
    } 
}

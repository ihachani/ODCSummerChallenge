﻿using System;
using System.Collections.Generic;
using UnityEngine;
using OrangeSummerChallenge.Navigation;

namespace OrangeSummerChallenge.Focus.Actions.TeleportingObjectToDestination
{
    [Serializable]
    public class TeleportObjectToDestinationAction : IFocusAction
    {
        [SerializeField]
        private Transform objectToTeleport;
        [SerializeField]
        private List<Transform> destinations;

        private int counter = 0;
        public void OnFocus(object sender, FocusEventArgs eventArgs)
        {
        }

        public void OnFocusComplete(object sender, FocusEventArgs eventArgs)
        {
            if (counter < destinations.Count)
            {
                objectToTeleport.position = destinations[counter].position;
                counter++;
            }
        }

        public void OnFocusLost(object sender, FocusEventArgs eventArgs)
        {
        }
    }
}

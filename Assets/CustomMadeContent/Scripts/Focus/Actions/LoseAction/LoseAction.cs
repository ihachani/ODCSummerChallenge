﻿using OrangeSummerChallenge.Stairs;
using OrangeSummerChallenge.Focus.Actions;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace OrangeSummerChallenge.Focus.Actions.LoseAction
{
    [Serializable]
    class LoseAction : IFocusAction
    {
        [SerializeField]
        private StairsMaster master;

        private bool exec = false;

        private string loseColliderName;

        public string LoseColliderName
        {
            get { return loseColliderName; }
            set { loseColliderName = value; }
        }
        public void OnFocus(object sender, OrangeSummerChallenge.Focus.FocusEventArgs eventArgs)
        {

        }

        public void OnFocusComplete(object sender, OrangeSummerChallenge.Focus.FocusEventArgs eventArgs)
        {
            if (!exec)
            {
                Debug.Log("lose collider focused" + loseColliderName);
                master.LoseGame();
                exec = true;

            }
        }

        public void OnFocusLost(object sender, OrangeSummerChallenge.Focus.FocusEventArgs eventArgs)
        {

        }
    }
}

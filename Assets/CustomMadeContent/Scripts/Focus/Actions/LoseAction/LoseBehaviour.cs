﻿using OrangeSummerChallenge.Focus;
using OrangeSummerChallenge.Focus.VR;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace OrangeSummerChallenge.Focus.Actions.LoseAction
{
    [RequireComponent(typeof(FocusableBehaviour))]
    [RequireComponent(typeof(GazableBehavior))]
    class LoseBehaviour : MonoBehaviour 
    {
        [SerializeField]
        private LoseAction loseAction;
        IFocusableBehavior focusable; 

        void Start() {

            focusable = GetComponent<FocusableBehaviour>();

            focusable.ObjectFocusComplete += loseAction.OnFocusComplete;
            loseAction.LoseColliderName=gameObject.name;
        
        }


    }
}

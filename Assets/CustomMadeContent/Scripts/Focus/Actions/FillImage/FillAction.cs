﻿using System;
using UnityEngine;
using UnityEngine.UI;
namespace OrangeSummerChallenge.Focus.Actions.FillImage
{
    [Serializable]
    class FillAction : IFocusAction
    {
        [SerializeField]
        private Image imageToFill;
        [SerializeField]
        private float fillAmount;
        public void OnFocus(object sender, FocusEventArgs eventArgs)
        {

        }

        public void OnFocusComplete(object sender, FocusEventArgs eventArgs)
        {
            imageToFill.fillAmount += fillAmount;
        }

        public void OnFocusLost(object sender, FocusEventArgs eventArgs)
        {
        }
    }
}

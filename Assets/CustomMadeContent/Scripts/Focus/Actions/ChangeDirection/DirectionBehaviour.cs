﻿using OrangeSummerChallenge.Focus.VR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace OrangeSummerChallenge.Focus.Actions.ChangeDirection
{
    [RequireComponent(typeof(FocusableBehaviour))]
    [RequireComponent(typeof(GazableBehavior))]

    class DirectionBehaviour : MonoBehaviour
    {
        [SerializeField]
        private DirectionAction dir;
        IFocusableBehavior focusable;

        void Start()
        {

            focusable = GetComponent<FocusableBehaviour>();

            focusable.ObjectFocusComplete += dir.OnFocusComplete;

        }
    }
}

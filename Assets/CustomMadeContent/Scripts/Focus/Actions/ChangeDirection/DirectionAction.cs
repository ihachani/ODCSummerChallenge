﻿using OrangeSummerChallenge.Actions;
using OrangeSummerChallenge.Behaviour;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


namespace OrangeSummerChallenge.Focus.Actions.ChangeDirection
{
    [Serializable]
    class DirectionAction : IFocusAction {

        [SerializeField]
        private AddForceBehaviour body;
        [SerializeField]
        private Vector3 direction;
        [SerializeField]
        private bool left;

        public void OnFocus(object sender, FocusEventArgs eventArgs)
        {
            throw new NotImplementedException();
        }

        public void OnFocusComplete(object sender, FocusEventArgs eventArgs)
        {
            Debug.Log("Focused");
            body.att.setNewDirection(direction,left);
        }

        public void OnFocusLost(object sender, FocusEventArgs eventArgs)
        {
            throw new NotImplementedException();
        }
    }
}

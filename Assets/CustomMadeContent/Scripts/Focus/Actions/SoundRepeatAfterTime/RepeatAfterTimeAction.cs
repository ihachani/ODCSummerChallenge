﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OrangeSummerChallenge.Focus.Actions.SoundRepeatAfterTime
{
    [Serializable]
    class RepeatAfterTimeAction : IFocusAction
    {
        [SerializeField]
        private AudioSource audio;

        [SerializeField]
        private MonoBehaviour mono; 

        private bool isPlaying=false;



        public void OnFocus(object sender, FocusEventArgs eventArgs)
        {
            throw new NotImplementedException();
        }

        public void OnFocusComplete(object sender, FocusEventArgs eventArgs)
        {
            while (!isPlaying)
            {
                mono.StartCoroutine(ResetAudio());
                isPlaying = true;
                audio.Play();

            }

        }

        IEnumerator ResetAudio() {
            yield return new WaitForSeconds(audio.clip.length+1f);
            isPlaying = false;
            yield return null;
        

        }

        public void OnFocusLost(object sender, FocusEventArgs eventArgs)
        {
            throw new NotImplementedException();
        }
    }
}
﻿using OrangeSummerChallenge.Focus.VR;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace OrangeSummerChallenge.Focus.Actions.SoundRepeatAfterTime
{
    [RequireComponent(typeof(FocusableBehaviour))]
    [RequireComponent(typeof(GazableBehavior))]

    class RepeatAfterTimeBehaviour : MonoBehaviour
    {
        [SerializeField]
        private RepeatAfterTimeAction repeat;
        IFocusableBehavior focusable ;

        void Start()
        {
            focusable = GetComponent<FocusableBehaviour>();

            focusable.ObjectFocusComplete += repeat.OnFocusComplete;
        }

    }
}

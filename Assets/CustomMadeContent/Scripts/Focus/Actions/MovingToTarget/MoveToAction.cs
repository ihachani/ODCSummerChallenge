﻿using System;
using UnityEngine;
using OrangeSummerChallenge.Navigation;

namespace OrangeSummerChallenge.Focus.Actions.MovingToTarget
{
    [Serializable]
    public class MoveToAction : IFocusAction
    {
        [SerializeField]
        private Transform destintaion;
        [SerializeField]
        private NavigationManager navigationManager;

        public void OnFocus(object sender, FocusEventArgs eventArgs)
        {
        }

        public void OnFocusComplete(object sender, FocusEventArgs eventArgs)
        {
            navigationManager.SetDestination(destintaion.position);
        }

        public void OnFocusLost(object sender, FocusEventArgs eventArgs)
        {
        }
    }
}

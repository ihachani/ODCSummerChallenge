﻿using UnityEngine;
using System.Collections;
using OrangeSummerChallenge.Focus.VR;

namespace OrangeSummerChallenge.Focus.Actions.ChangeSprite
{
    [RequireComponent(typeof(FocusableBehaviour))]
    [RequireComponent(typeof(GazableBehavior))]
    public class ChangeSpriteBehaviour : MonoBehaviour
    {

        [SerializeField]
        private ChangeSpriteAction changeSpriteAction;
        IFocusableBehavior focusable;

        void Start()
        {
            focusable = GetComponent<FocusableBehaviour>();

            focusable.ObjectFocusComplete += changeSpriteAction.OnFocusComplete;
            focusable.ObjectFocusLost += changeSpriteAction.OnFocusLost;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace OrangeSummerChallenge.Focus.Actions.ChangeSprite
{
    [Serializable]
    class ChangeSpriteAction : IFocusAction
    {
        [SerializeField]
        private Sprite newImage;
        [SerializeField]
        private Image imageToChange;
        private Sprite oldImage;
        public void OnFocus(object sender, FocusEventArgs eventArgs)
        {

        }

        public void OnFocusComplete(object sender, FocusEventArgs eventArgs)
        {
            oldImage = imageToChange.sprite;
            imageToChange.sprite = newImage;
        }

        public void OnFocusLost(object sender, FocusEventArgs eventArgs)
        {
            imageToChange.sprite = oldImage;
        }
    }
}

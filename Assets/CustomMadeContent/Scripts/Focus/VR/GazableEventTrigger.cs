﻿using UnityEngine;
using System.Collections;

namespace  OrangeSummerChallenge.Focus.VR
{
    /// <summary>
    /// Didn't work. Keep using the event trigger.
    /// </summary>
    [RequireComponent(typeof(GazableBehavior))]
    public class GazableEventTrigger : MonoBehaviour
    {
        private GazableBehavior gazableBehavior;
        void Start()
        {
            gazableBehavior = GetComponent<GazableBehavior>();
        }
        void OnMouseEnter()
        {
            Debug.Log("MouseEnter");
            gazableBehavior.SetGazedAt();
        }
        void OnMouseExit()
        {
            gazableBehavior.ResetGaze();
            gazableBehavior.StopCoroutine("FocusCouroutine");
        }
    }
    
}
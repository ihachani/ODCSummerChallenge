﻿using UnityEngine;
using System.Collections;
using OrangeSummerChallenge.Focus;
using UnityEngine.EventSystems;

namespace OrangeSummerChallenge.Focus.VR
{
    /// <summary>
    /// MonoBehavior that warps the google gaze.
    /// The methods are triggerd via an Event Trigger.
    /// </summary>
    [RequireComponent(typeof(FocusableBehaviour))]
    [RequireComponent(typeof(EventTrigger))]
    public class GazableBehavior : MonoBehaviour
    {
        FocusableBehaviour focusableBehavior;
        private bool gazedAt;

        public bool GazedAt
        {
            get { return gazedAt; }
            set { gazedAt = value; }
        }
        void Start()
        {
            focusableBehavior = GetComponent<FocusableBehaviour>();
        }

        public void SetGazedAt()
        {
            StartCoroutine("FocusCouroutine");
        }
        IEnumerator FocusCouroutine()
        {
            while (true)
            {
                focusableBehavior.Focused();
                yield return null;
            }

        }
        public void ResetGaze()
        {
            focusableBehavior.FocusLost();
        }


        public void ToggleVRMode()
        {
            Cardboard.SDK.VRModeEnabled = !Cardboard.SDK.VRModeEnabled;
        }
    }

}
﻿using UnityEngine;
using System.Collections;
using OrangeSummerChallenge.Focus;

namespace OrangeSummerChallenge.Focus
{
    /// <summary>
    /// Test Mono used to show focus events.
    /// </summary>
    public class TeleportAction : MonoBehaviour
    {



        //Local Variables 
        Renderer rend;
        IFocusableBehavior focusable;
        int Counter;
        [SerializeField]
        private GameObject teleportLocation;
        [SerializeField]
        private GameObject teleportLocation2;
        AudioSource voice;




        // Use this for initialization
        void Start()
        {

            focusable = GetComponent<FocusableBehaviour>();
            voice = gameObject.GetComponent<AudioSource>();
            AddFocusEventHandlers();
            Counter = 0;


        }

        private void AddFocusEventHandlers()
        {
            focusable.ObjectFocused += focusable_ObjectFocused;
            focusable.ObjectFocusComplete += focusable_ObjectFocusComplete;
            focusable.ObjectFocusLost += focusable_ObjectFocusLost;
        }
        void OnEnable()
        {
            rend = GetComponent<Renderer>();
        }

        void focusable_ObjectFocusLost(object sender, FocusEventArgs eventArgs)
        {
            Counter++;
            Debug.Log("Focus Lost : " + Counter);

        }

        void focusable_ObjectFocusComplete(object sender, FocusEventArgs eventArgs)
        {

        }

        void focusable_ObjectFocused(object sender, FocusEventArgs eventArgs)
        {

            if (Counter == 0)
            {

                StartCoroutine("WomanDispear");

            }

            if (Counter == 1)
            {
                StartCoroutine("WomanReappear");
                Counter++;
            }
        }
        // Update is called once per frame
        void Update()
        {

        }


        //Couroutines 
        //couroutine I
        IEnumerator WomanDispear()
        {


            Debug.Log("Couroutine1 Played");

            this.transform.position = teleportLocation.transform.position;


            voice.Play();
            yield return new WaitForSeconds(2f);



        }


        //couroutine II

        IEnumerator WomanReappear()
        {

            Debug.Log("Couroutine2 Played");
            this.transform.position = teleportLocation2.transform.position;


            voice.Play();
            yield return new WaitForSeconds(10f);




        }


    }

}

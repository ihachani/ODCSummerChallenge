﻿using UnityEngine;
using System.Collections;
using OrangeSummerChallenge.Actions;
namespace OrangeSummerChallenge.Managers
{
    public class Sahra1LoadScene : MonoBehaviour
    {
        [SerializeField]
        private LoadSceneAction loadSceneAction;
        void OnTriggerEnter(Collider other)
        {

            if (other.tag == "Player")
            {

                loadSceneAction.Action();
            }

            else
            {

                Debug.Log("Somthing But not Player Entred");

            }


        }
    }
}
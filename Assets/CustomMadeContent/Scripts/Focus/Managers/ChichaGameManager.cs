﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using OrangeSummerChallenge.Focus.Actions.NotifyChichaGameManager;
using OrangeSummerChallenge.Actions;

namespace OrangeSummerChallenge.Managers
{
    public class ChichaGameManager : MonoBehaviour
    {
        [SerializeField]
        private List<NotifyChichaGameManagerBehaviour> notifyChichaGameManagerBehaviours;
        [SerializeField]
        private float itemsNumber;
        [SerializeField]
        private LoadSceneAction loadSceneAction; 
        private float collectedItemsCounter = 0;
        [SerializeField]
        private AudioSource soundToWin; 
        // Use this for initialization
        void Start()
        {
            foreach (var notifyChichaGameManagerBehaviour in notifyChichaGameManagerBehaviours)
            {
                notifyChichaGameManagerBehaviour.chichaFocused += notifyChichaGameManagerBehaviour_chichaFocused;
            }
        }
        /// <summary>
        /// Handles chicha being focused
        /// Increments the counter.
        /// if <b>collectedItemsCounter</b> is reached launches An event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        void notifyChichaGameManagerBehaviour_chichaFocused(object sender, System.EventArgs eventArgs)
        {
            if (collectedItemsCounter < itemsNumber - 1)
            {
                collectedItemsCounter++;
            }
            else
            {
                if (soundToWin != null)
                {
                    soundToWin.Play();
                }
                StartCoroutine(WinCouroutine());
              
            }

        }


        IEnumerator WinCouroutine() {
            if (soundToWin != null)
            {
                yield return new WaitForSeconds(soundToWin.clip.length);
            } 
            loadSceneAction.Action();
            yield return null;
             
        }
    }

}
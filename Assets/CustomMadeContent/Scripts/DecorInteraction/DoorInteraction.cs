﻿using UnityEngine;
using System.Collections;

public class DoorInteraction : MonoBehaviour {

    private Animator anim;
    [SerializeField]
    private GameObject player;


	// Use this for initialization
	void Start () {

        anim = player.GetComponent<Animator>();
	}


    void OnTriggerEnter(Collider other) {

        if (other.tag == "Play") {


            StartVibration();
        
        }
    
    }
	
	// Update is called once per frame
    public void StartVibration() {
        anim.SetTrigger("VirationStart");
      
    }

    public void StopVibration() {

        anim.SetTrigger("VibrationStop");        
    }
}

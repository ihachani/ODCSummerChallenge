﻿using UnityEngine;
using System.Collections;
using OrangeSummerChallenge.Utility;
namespace OrangeSummerChallenge.ManualTests
{
    public class TestingAsyncLoad : MonoBehaviour
    {
        [SerializeField]
        private LoadAsync asyncLoader;
        // Use this for initialization
        void Start()
        {
            asyncLoader.LoadNextLevel();
        }

        // Update is called once per frame
        void Update()
        {

        }
    } 
}

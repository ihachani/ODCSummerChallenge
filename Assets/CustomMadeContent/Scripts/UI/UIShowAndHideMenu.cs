﻿using UnityEngine;
using System.Collections;

public class UIShowAndHideMenu : MonoBehaviour
{
    [SerializeField]
    private GameObject menuToHide;
    [SerializeField]
    private GameObject menuToShow;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void HideMenu()
    {
        menuToHide.SetActive(false);
    }
    public void ShowMenu()
    {
        menuToShow.SetActive(true);
    }
}

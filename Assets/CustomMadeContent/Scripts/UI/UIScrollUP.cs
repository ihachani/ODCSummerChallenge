﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace OrangeSummerChallenge.UI
{
    public class UIScrollUP : MonoBehaviour
    {
        [SerializeField]
        private RectTransform UIToScrollUp;
        [SerializeField]
        private float scrollSpeed = 1;
        // Use this for initialization
        void OnEnable()
        {
            UIToScrollUp.position = new Vector3(0, 0, 0);
        }
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            MoveUI_Up();
        }

        private void MoveUI_Up()
        {
            UIToScrollUp.position += new Vector3(0, scrollSpeed, 0);
        }
    }

}
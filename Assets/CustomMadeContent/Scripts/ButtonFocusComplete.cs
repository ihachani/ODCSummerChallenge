﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using OrangeSummerChallenge.Focus;

namespace OrangeSummerChallenge
{
    public class ButtonFocusComplete : MonoBehaviour
    {
        private IFocusableBehavior focusable;
        [SerializeField]
        private Text buttonText;
        // Use this for initialization
        void Start()
        {
            focusable = GetComponent<FocusableBehaviour>();
            AddFocusEventHandlers();
        }
        private void AddFocusEventHandlers()
        {
            focusable.ObjectFocused += focusable_ObjectFocused;
            focusable.ObjectFocusComplete += focusable_ObjectFocusComplete;
        }

        private void focusable_ObjectFocusComplete(object sender, FocusEventArgs eventArgs)
        {
            buttonText.text = "Completely Focused after: " + eventArgs.FocusTime;

        }

        private void focusable_ObjectFocused(object sender, FocusEventArgs eventArgs)
        {
            buttonText.text = "Focused For: " + eventArgs.FocusTime;

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}

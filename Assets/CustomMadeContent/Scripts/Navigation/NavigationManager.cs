﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace OrangeSummerChallenge.Navigation
{
    [Serializable]
    public class NavigationManager : INavigationManager
    {
        [SerializeField]
        NavMeshAgent navigationAgent;

        public void SetDestination(Vector3 destination)
        {
            navigationAgent.SetDestination(destination);
        }
        public void StopNavigation()
        {
            navigationAgent.Stop();
        }
        public void ContinueNavigation()
        {
            navigationAgent.Resume();
        }
        public void DisableNavagent()
        {
            navigationAgent.enabled = false;
        }
        public void EnableNavagent()
        {
            navigationAgent.enabled = true;
        }
    }
}

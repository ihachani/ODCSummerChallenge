﻿using System;
namespace OrangeSummerChallenge.Navigation
{
    public interface INavigationManager
    {
        void ContinueNavigation();
        void DisableNavagent();
        void EnableNavagent();
        void SetDestination(UnityEngine.Vector3 destination);
        void StopNavigation();
    }
}

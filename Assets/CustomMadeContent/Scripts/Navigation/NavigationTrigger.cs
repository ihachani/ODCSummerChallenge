﻿using UnityEngine;
using System.Collections;

namespace OrangeSummerChallenge.Navigation
{
    public class NavigationTrigger : MonoBehaviour
    {
        #region PrivateFields
        [SerializeField]
        private Transform teleportTarget;
        [SerializeField]
        private Transform newTarget; 
        #endregion

        #region UnityLifeCycleMethods
        void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "Player")
            {
                INavigationManager navigationManager = GetNavigationManagerFromCollidedObject(other);
                StartCoroutine(TriggerHitRoutine(navigationManager, other.transform));
            }
        } 
        #endregion

        #region Private Methods
        /// <summary>
        /// Returns the NavigationManager from the collider.
        /// </summary>
        /// <param name="collidedCollider">the collider</param>
        /// <returns>the colliders NavigationManager</returns>
        private INavigationManager GetNavigationManagerFromCollidedObject(Collider collidedCollider)
        {
            return collidedCollider.gameObject.GetComponent<SimpleNavigationBehavior>().NavigationManger;
        }
        IEnumerator TriggerHitRoutine(INavigationManager navigationManager, Transform ColliderTransform)
        {
            navigationManager.StopNavigation();
            navigationManager.DisableNavagent();
            yield return new WaitForSeconds(1f);
            ColliderTransform.position = teleportTarget.position;
            navigationManager.EnableNavagent();
            navigationManager.SetDestination(newTarget.position);
        } 
        #endregion
    }
}

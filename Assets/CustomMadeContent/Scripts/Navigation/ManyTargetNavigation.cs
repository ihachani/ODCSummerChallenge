﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
namespace OrangeSummerChallenge.Navigation
{
[RequireComponent(typeof(NavMeshAgent))]
public class ManyTargetNavigation : MonoBehaviour {
	#region Private Fields
	[SerializeField]
	private float duration=2f;
	private int count;
	private int size;
	private float dist;
	private List<Transform> targets;
	private Transform selectedTarget;
	NavMeshAgent navigationAgent;
	[SerializeField]
	NavigationManager navigationManger; 
	private bool reached;
	#endregion
	
	#region Properties
	public INavigationManager NavigationManger
	{
		get { return navigationManger; }
	} 
	#endregion
	
	void Start()
	{
		targets = new List<Transform>();
		count=0;
		AddAllTargets();
		selectedTarget =targets[count];
		size=targets.Count;
		Debug.Log ("size"+size);
	}

	void Update()
	{
		navigationManger.SetDestination(selectedTarget.position);
		dist=Vector3.Distance( this.transform.position, selectedTarget.position);
		//Debug.Log("distance"+dist);
		if(dist<2f)
		{
				count++;
			//StartCoroutine(ChangeTarget());
			if(count < size)
				{
					Debug.Log ("change target");
					selectedTarget=targets[count];
				}
		}
	}
	void AddAllTargets()
	{
		int i=0;
		GameObject[]go = GameObject.FindGameObjectsWithTag("target").OrderBy( z => z.name ).ToArray();    
		foreach(GameObject target in go)
		{
			Debug.Log("target"+i);
			i++;
			AddTarget(target.transform);
		}
	}
	public void AddTarget(Transform target){
		targets.Add(target);
	}
		public IEnumerator ChangeTarget()
		{
			reached=true;
			//count++;
			Debug.Log("couroutine");
			yield return new WaitForSeconds(duration);
		}
 }
}
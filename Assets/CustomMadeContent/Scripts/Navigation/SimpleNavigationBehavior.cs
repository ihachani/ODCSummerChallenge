﻿using UnityEngine;
using System.Collections;

namespace OrangeSummerChallenge.Navigation
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class SimpleNavigationBehavior : MonoBehaviour
    {
        
        #region Private Fields
        [SerializeField]
        Transform target2;
        [SerializeField]
        private Transform target;
        NavMeshAgent navigationAgent;
        [SerializeField]
        NavigationManager navigationManger; 
        

        private bool reached1;
        #endregion

        #region Properties
        public INavigationManager NavigationManger
        {
            get { return navigationManger; }
        } 
        #endregion
        
        void Start()
        {
           
            //Just for Testing.
            navigationManger.SetDestination(target.position);
            
        }








      

    } 
}

﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Animator))]
public class AnimtorPlayAfterAnotherAnimation : MonoBehaviour {
	private Animator animator;
	[SerializeField]
	private string triggerName;
	[SerializeField]
	private AnimtorPlayAfterAnotherAnimation nextAnimtorManager;
	[SerializeField]
	private GameObject menuElementToActivate;
	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
	}
	public void StartAnimating(){
		animator.SetTrigger (triggerName);
		StartCoroutine(AnimationCoroutine());
	}

	IEnumerator AnimationCoroutine(){
		while(animator.GetCurrentAnimatorStateInfo(0).IsName("Idle")){
			yield return null;
		}
		while (animator.GetCurrentAnimatorStateInfo(0).normalizedTime <= 0.95 && !animator.IsInTransition(0)) {
			yield return null;
		}
		if (nextAnimtorManager != null) 
			nextAnimtorManager.StartAnimating();
		menuElementToActivate.SetActive (true);
	}
}

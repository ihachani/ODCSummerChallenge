﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class FBHolder : MonoBehaviour {

    [SerializeField]
    private GameObject UIFBLoggedIn;
    [SerializeField]
    private GameObject UIFBNotLoggedIn;
    [SerializeField]
    private GameObject UIFBAvatar;
    [SerializeField]
    private GameObject UIFBUserName;


    private Dictionary<string, string> profile = null;


    void Awake() {

        FB.Init(SetInit, OnHide);
    
    }


    private void SetInit() {

        Debug.Log("FB Init Done...");

        if (FB.IsLoggedIn)
        {
          //  DealWithFBMenus(true);
            Debug.Log("User Logged In");
        }
        else {

           // DealWithFBMenus(false);
        
        }

    }


    private void OnHide(bool isGameShown) {

        if (!isGameShown)
        {

            Time.timeScale = 0;
        }
        else {

            Time.timeScale = 1;
        }

    }


    public void FBLogin() {

        FB.Login("email" , AuthCallBack);

    }

    private void AuthCallBack(FBResult fresult) {

        if (FB.IsLoggedIn)
        {
            ShareWithFreinds();
           // DealWithFBMenus(true);
            Debug.Log("User Logged In");
        }
        else {
          //  DealWithFBMenus(false);
            Debug.Log("There is no user logged in");

        }
    
    }

   



    public void DealWithProfilePicture(FBResult result) {
        if (result.Error != null) {

            Debug.Log("Problem With Getting Profile Picture");
            FB.API(Util.GetPictureURL("me", 128, 128), Facebook.HttpMethod.GET, DealWithProfilePicture);
            return;
        } 

            
            
            Image userAvatar = UIFBAvatar.GetComponent<Image>() ;
            userAvatar.sprite = Sprite.Create(result.Texture, new Rect(0, 0, 128, 128), new Vector2(0, 0));
    
    
    }



    void DealWithUserName(FBResult result) {

        if (result.Error != null)
        {

            Debug.Log("Problem With Getting Profile Picture");
            FB.API("/me?fields=id,first_name", Facebook.HttpMethod.GET, DealWithUserName);
            return;
        }

        profile = Util.DeserializeJSONProfile(result.Text);
        Text userNM = UIFBUserName.GetComponent<Text>();
        userNM.text = "hello ,"+ profile["first_name"] ;
   

    }

    void DealWithFBMenus(bool IsloggedIn) {

        if (IsloggedIn)
        {
            UIFBLoggedIn.SetActive(true) ;
            UIFBNotLoggedIn.SetActive(false) ;

            //Get Profile Picture 
            FB.API(Util.GetPictureURL("me" , 128 ,128), Facebook.HttpMethod.GET ,DealWithProfilePicture);
            FB.API("/me?fields=id,first_name,link", Facebook.HttpMethod.GET, DealWithUserName);
            //Get User Name


        }
        else { 

            UIFBLoggedIn.SetActive(false) ;
            UIFBNotLoggedIn.SetActive(true);
        
        }
    
    }



    public void ShareWithFreinds()
    {

        // Debug.Log("User : "+ profile["id"]);

        if (!FB.IsLoggedIn)
        {

            FBLogin();

        }

        if (FB.IsLoggedIn)
        {

            FB.Feed(

                linkCaption: "I Have Accomplished Abstraction",
                picture: "http://www.mditunis.org/wp-content/uploads/2015/09/Logo.jpg",
                linkName: "Try This Great Experience",
                link: "http://apps.facebook.com/" + FB.AppId
                );

        }
    }

}
